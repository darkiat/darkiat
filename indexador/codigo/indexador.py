#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import subprocess
from datetime import datetime
import os
from elasticsearch import Elasticsearch
import time
from utils import utils
import re
from OpenSSL import crypto


def guardar_HTTP(datosHost,es):
	pathCabeceras = "/usr/src/results/"+datosHost[0]+"/"+datosHost[1].lower()+"_"+datosHost[0]+"_"+datosHost[2]
	if os.path.exists(pathCabeceras):
		httpResultsFile = open(pathCabeceras,'r')	
		httpHeaders = httpResultsFile.read()
		httpResultsFile.close()
		
		# Get server data
		server = re.findall("[Ss]erver: (.*)",httpHeaders.strip())
		
		# Indexar los datos
		e1 = {
			'date': datosHost[8],
			'host': datosHost[0],
			'service': datosHost[1],
			'port': datosHost[2],
			'headers': httpHeaders,
			'server': server,
			'name': datosHost[3],
			'description': datosHost[4],
			'category': datosHost[5],
			'source': datosHost[6],
		}
		
		if not utils.putES(es,"datos_http",e1):
			print("Error de indexación",datosHost[0],datosHost[1])
		else:
			print("Se ha indexado",datosHost[0],"servicio",datosHost[1],"puerto",datosHost[2],int(datosHost[7]))

def guardar_HTTPs(datosHost, es):
	pathCabeceras = "/usr/src/results/"+datosHost[0]+"/"+datosHost[1].lower()+"_"+datosHost[0]+"_"+datosHost[2]+"-H"
	pathCertificados = "/usr/src/results/"+datosHost[0]+"/"+datosHost[1].lower()+"_"+datosHost[0]+"_"+datosHost[2]+"-C"
	if os.path.exists(pathCabeceras):
		# Cabeceras
		httpsHeadersFile = open(pathCabeceras,'r')	
		httpsHeaders = httpsHeadersFile.read()
		httpsHeadersFile.close()					
		
		# Certificados
		httpsCertsFile = open(pathCertificados,'r')	
		httpsCerts = httpsCertsFile.read()
		httpsCertsFile.close()					
		
		# Obtener datos del certificado		
		cert = crypto.load_certificate(crypto.FILETYPE_PEM, httpsCerts)
		certDumped = crypto.dump_certificate(crypto.FILETYPE_TEXT, cert)
		
		# Get server data
		server = re.findall("[Ss]erver: (.*)",httpsHeaders.strip())
		
		# Crear estructura endexar los datos							
		e1 = {
			'date': datosHost[8],
			'host': datosHost[0],
			'service': datosHost[1],
			'port': datosHost[2],
			'headers': httpsHeaders,
			'server': server,
			'certificate': httpsCerts,
			'certificate_txt': certDumped.decode(),
			'name': datosHost[3],
			'description': datosHost[4],
			'category': datosHost[5],
			'source': datosHost[6],
		}
		
		if not utils.putES(es,"datos_https",e1):
			print("Error de indexación",datosHost[0],datosHost[1])
		else:
			print("Se ha indexado",datosHost[0],"servicio",datosHost[1],"puerto",datosHost[2],int(datosHost[7]))
		
def guardar_SSH(datosHost,es):
	pathSsh = "/usr/src/results/"+datosHost[0]+"/"+datosHost[1].lower()+"_"+datosHost[0]+"_"+datosHost[2]
	if os.path.exists(pathSsh):
		sshFile = open(pathSsh,'r')	
		sshContent = sshFile.read()
		sshFile.close()
		
		# Bulk mode
		matchRemoteProtocol = re.findall("debug1: Remote protocol.*",sshContent)
		matchKEXAlgo = re.findall("debug2: KEX algorithms:.*",sshContent)
		matchHostKeyAlgo = re.findall("debug2: host key algorithms:.*",sshContent)
		matchCiphers = re.findall("debug2: ciphers stoc:.*",sshContent)
		matchMACs = re.findall("debug2: MACs stoc:.*",sshContent)
		matchCompression = re.findall("debug2: compression stoc:.*",sshContent)
		matchHostKey = re.findall("debug1: Server host key:.*",sshContent)
		
		# Comprobar que existen los campos esperados
		if(len(matchRemoteProtocol) == 1 and len(matchKEXAlgo) == 1 and len(matchHostKeyAlgo) == 1 and len(matchCiphers) == 1 and len(matchMACs) == 1 and len(matchCompression) == 1 and len(matchHostKey) == 1):
			
			# Patrones para encontrar datos relevantes
			sshVersionPattern	= "Remote protocol version (.*?), remote software version (.*)"
			kexAlgoPattern	= "debug2: KEX algorithms: (.*)"
			hostKeyAlgoPattern	= "debug2: host key algorithms: (.*)"
			ciphersPattern	= "debug2: ciphers stoc: (.*)"
			macsPattern	= "debug2: MACs stoc: (.*)"
			compressionPattern	= "debug2: compression stoc: (.*)"
			hostKeyPattern = "debug1: Server host key: (.*) (.*)"
			
			# Buscar todos los patrones 
			sshVersionResults = re.search(sshVersionPattern, matchRemoteProtocol[0].strip())
			kexAlgoResults = re.search(kexAlgoPattern, matchKEXAlgo[0].strip())
			keyAlgoResults = re.search(hostKeyAlgoPattern, matchHostKeyAlgo[0].strip())
			ciphersResults = re.search(ciphersPattern, matchCiphers[0].strip())
			macsResults = re.search(macsPattern, matchMACs[0].strip())
			compressionResults = re.search(compressionPattern, matchCompression[0].strip())
			hostKeyTypeResults = re.search(hostKeyPattern, matchHostKey[0].strip())
			
			# Construir el JSON cabecera, se irá completando a continuación
			e1 = {
				'date': datosHost[8],
				'host': datosHost[0],
				'service': datosHost[1],
				'port': datosHost[2],
				'name': datosHost[3],
				'description': datosHost[4],
				'category': datosHost[5],
				'source': datosHost[6],
			}

			# Si se ha encontrado la versión del protocolo y software, añadirlo para guardarlo en ES
			if sshVersionResults and len(sshVersionResults.groups()) == 2:
				sshVersion = sshVersionResults.groups()[0]
				sshSoftwareVersion = sshVersionResults.groups()[1]
				e1['ssh_version'] = sshVersion
				e1['ssh_software_version'] = sshSoftwareVersion
				e1['server'] = sshSoftwareVersion

			# Si se han encontrado datos de kex, añadirlos para guardarlo en ES
			if kexAlgoResults and len(kexAlgoResults.groups()) == 1:
				kexAlgo = kexAlgoResults.groups()[0]
				e1['kex_algo'] = kexAlgo
			
			# Si se han encontrado datos de algoritmos de claves, añadirlo para guardarlo en ES
			if keyAlgoResults and len(keyAlgoResults.groups()) == 1:
				keyAlgo = keyAlgoResults.groups()[0]
				e1['key_algo'] = keyAlgo

			# Si se han encontrado datos de algoritmos de cifrado, añadirlo para guardarlo en ES
			if ciphersResults and len(ciphersResults.groups()) == 1:
				ciphers = ciphersResults.groups()[0]
				e1['ciphers'] = ciphers

			# Si se han encontrado datos de algoritmos de mac, añadirlo para guardarlo en ES
			if macsResults and len(macsResults.groups()) == 1:
				macs = macsResults.groups()[0]
				e1['macs'] = macs

			# Si se han encontrado datos de compresión, añadirlo para guardarlo en ES
			if compressionResults and len(compressionResults.groups()) == 1:
				compression = compressionResults.groups()[0]
				e1['compression'] = compression
				
			# Si se han encontrado datos del tipo de clave y el fingerprint, añadirlo para guardarlo en ES
			if hostKeyTypeResults and len(hostKeyTypeResults.groups()) == 2:
				hostKeyType = hostKeyTypeResults.groups()[0]
				hostKeyFingerprint = hostKeyTypeResults.groups()[1]
				e1['host_key_type'] = hostKeyType
				e1['host_key_fingerprint'] = hostKeyFingerprint
				
			if not utils.putES(es,"datos_ssh",e1):
				print("Error de indexación",datosHost[0],datosHost[1])
			else:
				print("Se ha indexado",datosHost[0],"servicio",datosHost[1],"puerto",datosHost[2],int(datosHost[7]))
		else:	
			print(utils.FAIL+"ERROR"+utils.ENDC)


def guardar_FTP(datosHost,es):
	pathCabeceras = "/usr/src/results/"+datosHost[0]+"/"+datosHost[1].lower()+"_"+datosHost[0]+"_"+datosHost[2]
	if os.path.exists(pathCabeceras):
		httpResultsFile = open(pathCabeceras,'r')	
		httpHeaders = httpResultsFile.read()
		httpResultsFile.close()
		
		# Indexar los datos
		e1 = {
			'date': datosHost[8],
			'host': datosHost[0],
			'service': datosHost[1],
			'port': datosHost[2],
			'headers': httpHeaders,
			'name': datosHost[3],
			'description': datosHost[4],
			'category': datosHost[5],
			'source': datosHost[6],
		}
		
		if not utils.putES(es,"datos_ftp",e1):
			print("Error de indexación",datosHost[0],datosHost[1])		
		else:
			print("Se ha indexado",datosHost[0],"servicio",datosHost[1],"puerto",datosHost[2],int(datosHost[7]))
def main():
	startTime = time.time()
	print(utils.OKGREEN+"[START] "+str(startTime)+utils.ENDC)
	
	# Obteniendo variables de entorno
	esHost = os.getenv('ES_HOST','localhost')
	
	# Establecemos una única conexión con ES
	es = Elasticsearch(hosts=esHost)
	if not es.ping():
		print(utils.FAIL+"El cluster NO está funcionando en "+esHost)
		sys.exit()
	
	# Imprimir datos de ES
	print("-"*30)
	print('ES_HOST:',esHost+utils.OKGREEN,'UP'+utils.ENDC)
	print("-"*30)

	# Lectura del fichero con los resultados del escaneo
	inputFile = open('/usr/src/results/logs','r')	
	
	# Leer el fichero de entrada línea a línea
	for hostEscanear in inputFile.read().split('\n'):
		# Si la línea no está vacía, guardar host
		if len(hostEscanear) != 0:
			datosHost = hostEscanear.split(';')
			if (len(datosHost) == 9):
				# Que haya > 0 bytes
				if(int(datosHost[7]) > 0):
					if(datosHost[1].lower() == 'http'):
						guardar_HTTP(datosHost,es)
							
					elif(datosHost[1].lower() == 'https'):
						guardar_HTTPs(datosHost,es)
													
					elif(datosHost[1].lower() == 'ssh'):
						guardar_SSH(datosHost,es)
							
					elif(datosHost[1].lower() == 'ftp'):
						guardar_FTP(datosHost,es)
						
			else:
				print(utils.FAIL+"El host NO es válido"+utils.ENDC,hostEscanear)
	
	# Cerrar fichero de entrada
	inputFile.close()	
	
#Definición del menu principal
if __name__ == '__main__':
	main()