from datetime import datetime, timezone
import re
import subprocess
import os

#Colores
class utils:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'
	
	ONION_URL = '^[2-7a-z]{56}(.onion)?$|^[2-7a-z]{16}(.onion)?$'
	IP_PATTERN = '^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$'
	URL_PATTERN = '^[0-9a-zA-Z\-\.]{4,253}$'
	
	@staticmethod
	def getTime():
		# 2020-04-15 17:44:05
		return str(datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S"))	
		
	def putES(es,index,data):
		resultado = False
		try:
			res = es.index(index=index,body=data)
			if (res['result'] == 'created'):
				resultado = True
		except Exception as e:
			print("Error de indexación")
		return resultado

	def write_on_file(site, message):
		site.get_log_file().write(message)
		site.get_log_file().flush()
		
	def verify_host(host):
		result = False
		if re.search(utils.ONION_URL,host) or re.search(utils.IP_PATTERN,host):
			result = True
		return result
		
	def remove_folders(hostList):
		for host in hostList:
			if os.path.exists("/results/"+host.get_host()):
				subprocess.getoutput("rm -r /results/"+host.get_host())		
				
	def get_array(element, size):
		return [element] * size
	