# INDEXADOR
Este componente está pensado para indexar aquellos datos que están almacenados en el sistema de ficheros siguiendo la estructura en la que los generó el *colector*.

Permite el uso del *colector* en entornos reducidos en los que no se disponga de recursos para montar un clúster de ElasticSearch. Así pues, lo único que se necesita es exportar la carpeta de *resultados* que genera el *colector* tras su ejecución y posteriormente importarla con este componente.

## CONTENIDOS

+ [Arquitectura](#arquitectura)
	+ [Parámetros de entrada](#parámetros-de-entrada)
+ [Despliegue de la aplicación](#despliegue-de-la-aplicación)
	+ [Ejemplo de uso](#ejemplo-de-uso)

# ARQUITECTURA
Como se muestra en la siguiente ilustración, está compuesta de un único componente que reúne todas las dependencias necesarias para leer los datos del sistema de ficheros e indexarlos en ElasticSearch:
![indexador arch](png/indexador.png)

Como se puede observar en la ilustración anterior, el componente recoge la información del sistema de ficheros (información obtenida directamente tras la ejecución del *colector*, y la indexa atendiendo a la estructura predefinida en ElasticSearch. 

## PARÁMETROS DE ENTRADA

El indexador simplemente recibe un parámetro de entrada, que es el punto de entrada a ElasticSearch:

| Parámetro        | Descripción           | Valores posibles | Valor por defecto |
| ------------- |-------------|-------------|-------------|
| ES_HOST      | Punto de entrada al clúster/nodo de ElasticSearch | Dirección o IP. Ejemplos: micluster.midominio.com, 172.12.25.66 |localhost |


# DESPLIEGUE DE LA APLICACIÓN
Al igual que las demás aplicaciones, se puede desplegar con: `docker-compose up` o `docker-compose up -d` si se desea ejecutar en segundo plano.

Para indexar el contenido obtenido por el *colector* simplemente se necesita guardar en la carpeta *resultados* del indexador los datos de la carpeta *resultados* del *colector* y posteriormente ejecutar la aplicación como se muestra en el siguiente apartado:

## EJEMPLO DE USO
A continuación se muestra una captura de ejemplo de ejecución de la aplicación, en la que se puede observar el proceso de indexación:
![indexacion](png/indexacion.png)

La sintaxis mostrada es la siguiente: `Se ha indexado <host> servicio <servicio> puerto <puerto> <número de bytes>`

Adicionalmente, desde Kibana o a través de peticiones a ElasticSearch, se puede comprobar que los datos se han indexado correctamente:
![resultados_es](png/resultados_es.png)