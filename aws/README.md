# COLECTOR EN AWS
Dado que el componente colector es el que recoge toda la lógica principal, puede resultar habitual la necesidad de desplegarlo en cloud o en entornos remotos. Por ello se han desarrollado estos *playbooks* de [ansible](https://www.ansible.com/) con la intención de crear toda la infraestructura necesaria, desde cero, en [AWS Cloud](https://aws.amazon.com/) para el funcionamiento del colector.

## CONTENIDOS

+ [Arquitectura](#arquitectura)
	+ [Parámetros de entrada](#parámetros-de-entrada)
+ [Creación del entorno](#creación-del-entorno)

# ARQUITECTURA
Este conjunto de instrucciones Ansible crearán desde cero un entorno de ejecución en el que se pondrá en marcha el colector. El entorno creado está compuesto por los siguientes componentes:
+	[VPC](https://aws.amazon.com/vpc/): Virtual Private Cloud. Se trata de una porción aislada de forma lógica en la nube de AWS en la que se lanzarán los recursos
+	[Internet gateway](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html): componente que permite el enrutado del tráfico de la instancia hacia el exterior.
+	[Red pública](https://docs.aws.amazon.com/vpc/latest/userguide/working-with-vpcs.html): red en la que se conectará la instancia
+	[Instancia EC2](https://aws.amazon.com/es/ec2/): se trata de una máquina virtual sobre la que se ejecutará el colector.

![aws arch](png/aws.png)

Al tratarse de un escenario sencillo no se ha desarrollado la solución en alta disponibilidad ni en varias zonas de disponibilidad, por lo que todo el despliegue se hará en una única subred, una instancia y una zona de disponibilidad.

Como se muestra en el siguiente apartado, el usuario tiene la capacidad de elegir los parámetros de despliegue del entorno (nombres, direcciones IP, versiones de sistema operativo...)

## PARÁMETROS DE ENTRADA

El entorno desplegado contiene multitud de parámetros que se pueden adaptar en función de las necesidades del usuario. Esto se llevará a cabo haciendo uso de los [vault de Ansible](https://docs.ansible.com/ansible/latest/user_guide/vault.html), que no son más que ficheros que permiten mantener de forma segura datos sensibles como contraseñas, claves...
El fichero de variables puede editarse con el siguiente comando: `ansible-vault edit variables.yml`

Los parámetros, **todos obligatorios**, son los siguientes:

|      Parámetro        | Descripción           | 
| -------------         |-------------          |
| aws_access_key_id     | ID de clave de amazon. Para más info consultar [aquí](https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html) |
| aws_secret_access_key | Clave secreta de amazon. Para más info consultar [aquí](https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html) |
| aws_session_token     | Token de sesión para obtener permisos en amazon. Para más info consultar [aquí](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_temp_use-resources.html) |
| vpc_name              | Nombre del VPC que se creará |
| vpc_cidr              | Direccionamiento, en formato CIDR del VPC que se creará. Ejemplo: 10.120.20.0/24|
| region                | Región en la que crear los recursos |
| subnet_name           | Nombre de la subred pública en la que se desplegará la instancia con el colector|
| subnet_cidr           | Direccionamiento, en formato CIDR de la subred |
| subnet_az             | Zona de disponibilidad en la que se desplegará el entorno |
|route_table_name      	| Nombre de la tabla de rutas |
| instance_key          | Clave ssh de la instancia. La clave debe existir en el sistema de ficheros. Por ejemplo, si el usuario pone el parámetro mykey, la clave **pública** debe estar guardada en el fichero mykey.pub|
| instance_type         | Tipo de instancia. Por ejemplo: a1.medium	 |
| ami_id                | Identificador de la imagen a usar. El usuario podrá elegir el sistema operativo Linux que más le interese. Se ha probado con Amazon Linux 2. |
| device_name           | Nombre del dispositivo en el que se montará el disco de la instancia |
| volume_size           | Tamaño, en GiB del disco |
| volume_type           | Tipo de disco. (HDD,SDD... según la nomenclatura de AWS) |
| instance_name         | Nombre de la instancia |
| instance_user         | Usuario de la instancia. Necesario para poder entrar por SSH una vez desplegada, a instalar docker, colector...|
| sg_name               | Nombre del security group|
| sg_description        | Descripción del security group |
| user_ip               | IP del usuario: para aceptar las conexiones SSH desde dicha IP. Si el usuario quiere abrirlo a todo el mundo puede poner 0.0.0.0/0 |
|ansible_host_key_checking| Deshabilitar la verificación de claves SSH |

A continuación se incluye un ejemplo del fichero *variables.yml*:
```
---
# Variable files deploy_aws
aws_access_key_id: <your_access_key_id>
aws_secret_access_key: <your_secret_access_key>
aws_session_token: <your_session_token>

vpc_name: "colector-vpc"
vpc_cidr: "10.120.20.0/24"
region: "us-east-1"

subnet_name: "colector_subnet_001"
subnet_cidr: "10.120.20.16/28"
subnet_az: "us-east-1a"

route_table_name: "colector_route_table"

instance_key: "colector-key"
instance_type: "a1.medium"
ami_id: "ami-0323c3dd2da7fb37d"
device_name: "/dev/sdb"
volume_size: 15
volume_type: "standard"
instance_name: "colector-linux"
instance_user: "ec2-user"

sg_name: "colector-sg"
sg_description: "colector security group description"
user_ip: "0.0.0.0/0"

ansible_host_key_checking: False
```

# CREACIÓN DEL ENTORNO

Para crear el entorno bastará con ejecutar el playbook `deploy_aws.yml`.
Para ello, se lanzará el siguiente comando:

`ansible-playbook deploy_aws.yml -i inventory --ask-vault-pass` 

o 

`ansible-playbook deploy_aws.yml -i inventory --ask-vault-pass -v` si se desea lanzar en modo *verbose*.