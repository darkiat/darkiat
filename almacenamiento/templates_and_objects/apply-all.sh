bash apply-http-template.sh
bash apply-https-template.sh
bash apply-ssh-template.sh
bash apply-ftp-template.sh
bash import-kibana-objects.sh

curl -XPUT 'localhost:9200/datos_http'
curl -XPUT 'localhost:9200/datos_https'
curl -XPUT 'localhost:9200/datos_ftp'
curl -XPUT 'localhost:9200/datos_ssh'
curl -XPUT 'localhost:9200/cve_details'
curl -XPUT 'localhost:9200/software_cve'
