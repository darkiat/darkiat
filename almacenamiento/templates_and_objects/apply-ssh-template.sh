curl -X PUT "localhost:9200/_template/ssh_template?pretty" -H 'Content-Type: application/json' -d'
{
    "order" : 0,
    "version" : 1,
    "index_patterns" : [
      "datos_ssh"
    ],
    "settings" : { },
    "mappings" : {
      "properties" : {
        "date" : {
          "format" : "yyyy-MM-dd HH:mm:ss",
          "type" : "date"
        },
        "port" : {
          "type" : "short"
        },
        "name" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 100,
              "type" : "keyword"
            }
          }
        },
        "description" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 256,
              "type" : "keyword"
            }
          }
        },
        "service" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 25,
              "type" : "keyword"
            }
          }
        },
        "host" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 40,
              "type" : "keyword"
            }
          }
        },
        "ciphers" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 25,
              "type" : "keyword"
            }
          }
        },
        "compression" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 25,
              "type" : "keyword"
            }
          }
        },
        "kex_algo" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 25,
              "type" : "keyword"
            }
          }
        },
        "key_algo" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 25,
              "type" : "keyword"
            }
          }
        },
        "host_key_fingerprint" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 25,
              "type" : "keyword"
            }
          }
        },
        "host_key_type" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 25,
              "type" : "keyword"
            }
          }
        },
        "macs" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 25,
              "type" : "keyword"
            }
          }
        },
        "server" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 256,
              "type" : "keyword"
            }
          }
        },
        "ssh_software_version" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 256,
              "type" : "keyword"
            }
          }
        },
        "ssh_version" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 25,
              "type" : "keyword"
            }
          }
        },
        "category" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 256,
              "type" : "keyword"
            }
          }
        },
        "source" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 256,
              "type" : "keyword"
            }
          }
        }
      }
    },
    "aliases" : { }
}
'
