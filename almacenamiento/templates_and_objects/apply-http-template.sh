curl -X PUT "localhost:9200/_template/http_template?pretty" -H 'Content-Type: application/json' -d'
{
    "order" : 0,
    "version" : 1,
    "index_patterns" : [
      "datos_http"
    ],
    "settings" : { },
    "mappings" : {
      "properties" : {
        "date" : {
          "format" : "yyyy-MM-dd HH:mm:ss",
          "type" : "date"
        },
        "headers" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 256,
              "type" : "keyword"
            }
          }
        },
        "name" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 100,
              "type" : "keyword"
            }
          }
        },
        "description" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 256,
              "type" : "keyword"
            }
          }
        },
        "port" : {
          "type" : "short"
        },
        "service" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 25,
              "type" : "keyword"
            }
          }
        },
        "host" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 40,
              "type" : "keyword"
            }
          }
        },
        "category" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 40,
              "type" : "keyword"
            }
          }
        },
        "source" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 40,
              "type" : "keyword"
            }
          }
        },
        "server" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "ignore_above" : 256,
              "type" : "keyword"
            }
          }
        }
      }
    },
    "aliases" : { }
}
'
