ElasticSearch and Kibana are open-source products from [Elastic](https://www.elastic.co) developed under Apache 2.0 License. It is user responsability to comply with that licenses.

# Almacenamiento
Esta parte de la aplicación se encarga del almacenamiento de los datos en [ElasticSearch](https://www.elastic.co/elasticsearch/).
Además, se puede desplegar de forma opcional en un contenedor el componente [Kibana](https://www.elastic.co/kibana) que permitirá la búsqueda y visualización de datos de forma sencilla.

La arquitectura del componente de almacenamiento se muestra a continuación:

![Almacenamiento arch](png/almacenamiento.png)

Se desplegarán dos contenedores:
1.  Un único contenedor con ElasticSearch --> componente de almacenamiento.
2.  Un contenedor de Kibana --> componente de visualización
Para una prueba de concepto puede resultar más que suficiente. Sin embargo, resulta **importante** remarcar que si se desea disponer de un clúster de ElasticSearch en producción será necesario desplegar más *instancias*, en este caso contenedores, para permitir la creación y distribución de réplicas en el clúster. Véase el [apartado de ElasticSearch en HA](#elasticsearch-ha)


# Despliegue de la aplicación
Este conjunto de componentes se despliegan con el siguiente comando:
```docker-compose up``` o ```docker-compose up -d``` si se desea ejecutar en segundo plano.

Como se muestra a continuación, los mensajes de log indicarán la creación de los recursos definidos: red, volumen y contenedores.

![starting_compose arch](png/starting_compose.png)

Nota: si el usuario obtiene el siguiente error ```ERROR: [1] bootstrap checks failed --> max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]``` necesitará aumentar el límite máximo de memoria de su sistema, con ```sysctl -w vm.max_map_count=262144```

## Comprobar funcionamiento
Una forma sencilla de comprobar que ElasticSearch está listo es lanzando una petición HTTP al puerto 9200:
![es arch](png/es.png)

De forma análoga, lanzando una petición HTTP al puerto 5601 se podrá ver cuando Kibana está listo:
![es arch](png/kibana.png)

Una vez ambos servicios están en marcha, es necesario realizar las siguientes operaciones:
1.  Indicarle a ElasticSearch cómo debe tratar los datos que se van a indexar. De qué tipo son, formatos, tamaños... Es lo que se llama *mapping*. Los mappings necesarios están definidos en los ficheros `apply-<service>-template.sh`, de forma que se pueden añadir ejecutando los comandos [curl](https://curl.haxx.se/) que aparecen ahí. Existe un mapping diferente para cada servicio.
2.  Importar los patrones de indexación, visualizaciones y demás elementos en Kibana. Ahorrará tiempo el no tener que crearlos. Se puede hacer con el siguiente comando `bash import-kibana-objects.sh`

El paso 1 y el 2 se pueden aplicar en su conjunto ejecutando el siguiente comando `bash apply-all.sh` dentro de la carpeta `templates_and_objects`:
 
![import-data](png/import-data.png)

Una vez se hayan importado las plantillas de mapeo de datos y los elementos de visualización en Kibana deben aparecer patrones de indexación, elementos de visualización y dashboards (sin datos aún)

![import-data](png/dashboard.png)

El componente de almacenamiento ya está listo para recibir datos

# ElasticSearch HA
Atendiendo a la [documentación oficial](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html), un escenario en el que se desplieguen tres contenedores de ElasticSearch se define en el siguiente docker-compose:

```
version: '3.7'
services:
  darkiat_es01:
    image: docker.elastic.co/elasticsearch/elasticsearch:7.8.0
    container_name: darkiat_es01
    environment:
      - node.name=darkiat_es01
      - discovery.seed_hosts=darkiat_es02,darkiat_es03
      - cluster.initial_master_nodes=darkiat_es01,darkiat_es02,darkiat_es03
      - cluster.name=Darkiat-cluster
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
      - node.attr.rack=cpd1
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - esdata01:/usr/share/elasticsearch/data
    ports:
      - 9200:9200
    networks:
      - darkiat_storage_net:
  darkiat_es02:
    image: docker.elastic.co/elasticsearch/elasticsearch:7.8.0
    container_name: darkiat_es02
    environment:
      - node.name=darkiat_es02
      - discovery.seed_hosts=darkiat_es01,darkiat_es03
      - cluster.initial_master_nodes=darkiat_es01,darkiat_es02,darkiat_es03
      - cluster.name=Darkiat-cluster
      - bootstrap.memory_lock=true
      - node.attr.rack=cpd2
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - esdata02:/usr/share/elasticsearch/data
    networks:
      - darkiat_storage_net:
  darkiat_es03:
    image: docker.elastic.co/elasticsearch/elasticsearch:7.8.0
    container_name: darkiat_es03
    environment:
      - node.name=darkiat_es03
      - discovery.seed_hosts=darkiat_es01,darkiat_es02
      - cluster.initial_master_nodes=darkiat_es01,darkiat_es02,darkiat_es03
      - cluster.name=Darkiat-cluster
      - bootstrap.memory_lock=true
      - node.attr.rack=cpd3
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - esdata03:/usr/share/elasticsearch/data
    networks:
      - darkiat_storage_net:

      
  darkiat_kibana:
    image: docker.elastic.co/kibana/kibana:7.8.0
    container_name: darkiat_kibana
    environment:
      - ELASTICSEARCH_HOSTS=http://darkiat_es01:9200
    ports:
      - 5601:5601
    networks:
      - darkiat_storage_net:
         
volumes:
  esdata01:
    driver: local
  esdata02:
    driver: local
  esdata03:
    driver: local

networks:
  darkiat_storage_net:
    driver: bridge
    ipam:
      driver: default
      config:
        - subnet: 172.80.5.0/24
```

De esta forma, modificando el fichero `docker-compose.yml` original propuesto en este repositorio, se podría disponer de un clúster de ElasticSearch compuesto por tres nodos que permita alta disponibilidad y creación de réplicas, entre otras funciones.

En [docker-compose.yml_cluster](docker-compose.yml_cluster) se encuentra definido un ejemplo propuesto para desplegar 3 instancias de ElasticSearch y Kibana.
