<?php
/*
*	CÓDIGO CON FUNCIONES GENERALES
*	PARA LA APLICACIÓN
*/	

require_once('vendor/autoload.php');

/*
* Imprime el menú de la página
*/
function pintarMenu(){
	echo '
<nav class="navbar navbar-dark bg-dark justify-content-between">


        <a class="navbar-brand" href="/">
		<img src="darkiat_fav.png" width="30" height="30" class="d-inline-block align-top" loading="lazy">Inicio
	</a>


	<form class="form-inline" method="get">
		<input class="form-control mr-sm-2" type="search" placeholder="Search" name="s_id" id="s_id" aria-label="Search">
		<div class="form-group form-check-inline">
			<input type="checkbox" class="form-check-input" id="exactMatch" name="exactMatch" checked>
			<label class="form-check-label" for="exactMatch" style="color:white">Búsqueda exacta</label>
		</div>
		<select class="custom-select my-1 mr-sm-2" id="resultsNumber" name="resultsNumber">
			<option selected>10</option>
			<option value="15">15</option>
			<option value="25">25</option>
			<option value="50">50</option>
			<option value="100">100</option>
			<option value="200">200</option>
		</select>
		<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
	</form>
</nav>';
}
/*
* Imprime el pie de página
*/
function pintarPie(){
	echo '
<div class="jumbotron text-center" style="margin-bottom:0">
	<p><strong>Daniel Moreno Belinchón</strong>
	<br/>Darkiat v1 - Julio 2020
	<br/><a href="https://gitlab.com/darkiat">https://gitlab.com/darkiat</a></p>
</div>';
}

/*
* Imprime fecha y hora
*/
function pintarScriptReloj(){
	echo '
<script>
	function startFechaHora() {
		var today = new Date();
		var h = today.getHours();
		var m = today.getMinutes();
		var s = today.getSeconds();
		m = checkTime(m);
		s = checkTime(s);
		 document.getElementById("reloj").innerHTML =
		h + ":" + m + ":" + s;
		
		var dia = today.getDate();
		dia = checkTime(dia);
		var mes = today.getMonth() + 1;
		mes = checkTime(mes);
		var year = today.getFullYear();
	   
		document.getElementById("fecha").innerHTML =
		dia + "-" + mes + "-" + year;
		var t = setTimeout(startFechaHora, 500);
	}
	function checkTime(i) {
		if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
		return i;
	}
</script>';	
	
}
/*
* Función encargada de realizar búsquedas en ES. 
* Parámetros recibidos:
*	- index: índice en el que buscar
*	- query: consulta a realizar
*	- resultsNumber: número de resultados a devolver
*	- exactMatch: si se desea aplicar wildcard o no
*/
function buscarES($index,$query,$resultsNumber=10,$exactMatch=False){
	$results = '';
	$hosts = getESHost();
	// Crear conexión
	$client = Elasticsearch\ClientBuilder::create()
                    ->setHosts($hosts)
                    ->build();
	$scapedQuery = escaparQuery($query);
	if(!$exactMatch)
		$scapedQuery = '*'.$scapedQuery.'*';
	$params = [
		'index' => $index,
		'body'  => [
			'size' => $resultsNumber,
			'query' => [
				'query_string' => [
					'query' => $scapedQuery,
					'default_operator' => 'AND'
				]
			]
		]
	];
	// Hacer búsqueda
	$results = $client->search($params);
	return $results;
}
/*
* Función encargada de buscar el host en ES. 
* Parámetros recibidos:
*	- index: índice en el que buscar
*	- host: host a buscar
*/
function buscarHostES($index,$host){
	$results = '';
	$hosts = getESHost();
	// Crear conexión
	$client = Elasticsearch\ClientBuilder::create()
                    ->setHosts($hosts)
                    ->build();
	$params = [
		'index' => $index,
		'body'  => [
			'size' => 200,
			'query' => [
				'match_phrase' => [
					'host' => $host
				]
			]
		]
	];
	// Hacer búsqueda
	$results = $client->search($params);
	
	return $results;
}
/*
* Función encargada de buscar CVE asociadas a un software específico
* Parámetros recibidos:
*	- index: índice en el que buscar
*	- software: software a buscar
*/
function buscarSoftwareCVEES($index,$software){
	
	if(is_array($software)){
		$server = $software[0];
	}else
		$server = $software;
	
	if(!empty($server)){
		$results = array();
		$hosts = getESHost();
		// Crear conexión
		$client = Elasticsearch\ClientBuilder::create()
						->setHosts($hosts)
						->build();
		$params = [
			'index' => $index,
			'body'  => [
				'size' => 200,
				'query' => [
					'term' => [
						'software.keyword' => [
							'value' => $server
						]
					]
				]
			]
		];	
		
		// Hacer búsqueda
		$results = $client->search($params);
	}
	return $results;
}
/*
* Función encargada de buscar software asociadas a una CVE específico
* Parámetros recibidos:
*	- index: índice en el que buscar
*	- cve: cve a buscar
*/
function buscarCVESoftwareES($index,$cve){
	if(!empty($cve)){
		$results = array();
		$hosts = getESHost();
		// Crear conexión
		$client = Elasticsearch\ClientBuilder::create()
						->setHosts($hosts)
						->build();
		$params = [
			'index' => $index,
			'body'  => [
				'size' => 200,
				'query' => [
					'term' => [
						'cve_id.keyword' => [
							'value' => $cve
						]
					]
				]
			]
		];	
		
		// Hacer búsqueda
		$results = $client->search($params);
	}
	return $results;
}
/*
* Función encargada de buscar los detalles de una CVE dada
* Parámetros recibidos:
*	- index: índice en el que buscar
*	- cveIds: ID de la CVE a buscar
*/
function buscarDetallesCVEs($index,$cveIds){
	$results = array();
	$hosts = getESHost();
	// Crear conexión
	$client = Elasticsearch\ClientBuilder::create()
					->setHosts($hosts)
					->build();
	$params = [
		'index' => $index,
		'body'  => [
			'size' => 200,
			'query' => [
				'terms' => [
					'cve_id.keyword' => $cveIds
				]
			]
		]
	];	
	// Hacer búsqueda
	$results = $client->search($params);

	return $results;
}
/*
* Función encargada de escapar ciertos carácteres de acuerdo
* a la documentación oficial de ES, accesible en:
* https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html
*/
function escaparQuery($input){
	$specials = array('\\','/','=','&&','||','>','<','!','(',')','{','}','[',']','^','"','~','?',':','+','-');
	// Se le añade una barra invertida
	$replacement = '\\';
	$replaced = $input;
	foreach($specials as $letter){
		$replaced = str_replace($letter, $replacement.$letter, $replaced);
	}
	return $replaced;
}
/*
* Función encargada de imprimir un host en formato HTML 
* para ser mostrado una vez se hace la búsqueda
* Parámetros recibidos:
*	- host: host a imprimir
*/
function printHost($host){
	echo '<div class="card" style="">
  <div class="card-body">
    <h5 class="card-title"><a href="./?host='.$host['_source']['host'].'">'.$host['_source']['host'].'</a></h5>';
	if(!empty($host['_source']['name']))
		echo '<h6 class="card-subtitle mb-2 text-muted">'.$host['_source']['name'].'</h6>';
	if(!empty($host['_source']['description']))
		echo '<p class="card-text"><strong>Descripción:</strong> '.$host['_source']['description'].'</p>';
	if(!empty($host['_source']['category']))
		echo '<p class="card-text"><strong>Categoría:</strong> '.$host['_source']['category'].'</p>';
	
	if (strpos($host['_source']['service'], 'HTTPS') !== false) {
		echo '<span class="badge badge-primary">';
	}else if (strpos($host['_source']['service'], 'HTTP') !== false) {
		echo '<span class="badge badge-secondary">';
	}else if (strpos($host['_source']['service'], 'SSH') !== false) {
		echo '<span class="badge badge-info">';
	}else if (strpos($host['_source']['service'], 'FTP') !== false) {
		echo '<span class="badge badge-dark">';
	}
	
	echo $host['_source']['service'].'</span>';
  echo' </div>
</div>
';
}
/*
* Función encargada de imprimir los datos HTTP de un host en
* formato HTML para ser mostrado una vez se hace la búsqueda
* Parámetros recibidos:
*	- host: host a imprimir
*/
function pintarHostHttp($host){
	$salida = '<h3 id="http_service">HTTP/'.$host['hits']['hits'][0]['_source']['port'].'</h3>';
	
	if(is_array($host['hits']['hits'][0]['_source']['server'])){
		$server = $host['hits']['hits'][0]['_source']['server'][0];
	}else
		$server = $host['hits']['hits'][0]['_source']['server'];
	
	$salida .= '<pre><strong>Headers:</strong></br>'.$host['hits']['hits'][0]['_source']['headers'];
	if(!empty($server)){
		$salida .= '<strong>Server:</strong> '.$server.'</br>';
	}
	$salida .= '</pre>';
	
	return $salida;
	
}
/*
* Función encargada de imprimir los datos HTTPs de un host en
* formato HTML para ser mostrado una vez se hace la búsqueda
* Parámetros recibidos:
*	- host: host a imprimir
*/
function pintarHostHttps($host){
	$salida = '<h3 id="https_service">HTTP/'.$host['hits']['hits'][0]['_source']['port'].'</h3>';

	if(is_array($host['hits']['hits'][0]['_source']['server'])){
		$server = $host['hits']['hits'][0]['_source']['server'][0];
	}else
		$server = $host['hits']['hits'][0]['_source']['server'];
	
	$salida .= '<pre><strong>Headers:</strong></br>'.$host['hits']['hits'][0]['_source']['headers'].
	'<strong>Certificate (hr):</strong></br>'.$host['hits']['hits'][0]['_source']['certificate_txt'].
	'<strong>Certificate:</strong></br>'.$host['hits']['hits'][0]['_source']['certificate'];
	if(!empty($server)){
		$salida .= '<strong>Server:</strong> '.$server.'</br>';
	}
	$salida .= '</pre>';
	
	return $salida;
}
/*
* Función encargada de imprimir los datos SSH de un host en
* formato HTML para ser mostrado una vez se hace la búsqueda
* Parámetros recibidos:
*	- host: host a imprimir
*/
function pintarHostSsh($host){
	$salida = '<h3 id="ssh_service">SSH/'.$host['hits']['hits'][0]['_source']['port'].'</h3>';
	
	if(is_array($host['hits']['hits'][0]['_source']['server'])){
		$server = $host['hits']['hits'][0]['_source']['server'][0];
	}else
		$server = $host['hits']['hits'][0]['_source']['server'];
		
	$salida .= '<pre>';
	$salida .= '<strong>SSH software:</strong> '.$host['hits']['hits'][0]['_source']['ssh_software_version'];
	$salida .= '</br><strong>SSH version:</strong> '.$host['hits']['hits'][0]['_source']['ssh_version'];
	$salida .= '</br><strong>Key type::</strong> '.$host['hits']['hits'][0]['_source']['host_key_type'];
	$salida .= '</br><strong>Key fingerprint:</strong> '.$host['hits']['hits'][0]['_source']['host_key_fingerprint'];
	$salida .= '</br></br><strong>Kex Algorithms:</strong></br>';
	
	$kexAlgoItems = explode(',', $host['hits']['hits'][0]['_source']['kex_algo']);
	foreach ($kexAlgoItems as $ki)
		$salida .= '    '.$ki.'</br>';
	
	$salida .= '</br><strong>Server Host Key Algorithms:</strong></br>';
	$keyAlgoItems = explode(',', $host['hits']['hits'][0]['_source']['key_algo']);
	foreach ($keyAlgoItems as $ki)
		$salida .= '    '.$ki.'</br>';
	
	$salida .= '</br><strong>Encryption Algorithms:</strong></br>';
	$ciphersAlgoItems = explode(',', $host['hits']['hits'][0]['_source']['ciphers']);
	foreach ($ciphersAlgoItems as $ki)
		$salida .= '    '.$ki.'</br>';
		
	$salida .= '</br><strong>MAC Algorithms:</strong></br>';
	$macAlgoItems = explode(',', $host['hits']['hits'][0]['_source']['macs']);
	foreach ($macAlgoItems as $ki)
		$salida .= '    '.$ki.'</br>';			
	
	$salida .= '</br><strong>Compression Algorithms:</strong></br>';
	$compressionAlgoItems = explode(',', $host['hits']['hits'][0]['_source']['compression']);
	foreach ($compressionAlgoItems as $ki)
		$salida .= '    '.$ki.'</br>';			
	
	$serverCVE = array();
	if(!empty($server)){
		$salida .= '<strong>Server:</strong> '.$server.'</br>';
	}
	$salida .= '</pre>';
	
	return $salida;
}
/*
* Función encargada de imprimir los datos FTP de un host en
* formato HTML para ser mostrado una vez se hace la búsqueda
* Parámetros recibidos:
*	- host: host a imprimir
*/
function pintarHostFtp($host){
	$salida = '<h3 id="ftp_service">FTP/'.$host['hits']['hits'][0]['_source']['port'].'</h3>';
	
	if(is_array($host['hits']['hits'][0]['_source']['server'])){
		$server = $host['hits']['hits'][0]['_source']['server'][0];
	}else
		$server = $host['hits']['hits'][0]['_source']['server'];
	
	$salida .= '<pre>';
	$salida .= '<strong>Headers:</strong></br>'.$host['hits']['hits'][0]['_source']['headers'];
	if(!empty($server)){
		$salida .= '<strong>Server:</strong> '.$server.'</br>';
	}
	$salida .= '</pre>';
	
	return $salida;
}
/*
* Función encargada de devolver el color correspondiente
* a una puntuación de CVE, de acuerdo a cvedetails.com
* Parámetros recibidos:
*	- score: puntuación
*/
function getCVEColor($score){
	$color = '#FFFFFF';
	if($score <= 1){
		$color = '#00C400';
	}else if($score <= 2){
		$color = '#00E020';
	}else if($score <= 3){
		$color = '#00F000';
	}else if($score <= 4){
		$color = '#D1FF00';
	}else if($score <= 5){
		$color = '#FFE000';
	}else if($score <= 6){
		$color = '#FFCC00';
	}else if($score <= 7){
		$color = '#FFBC10';
	}else if($score <= 8){
		$color = '#FF9C20';
	}else if($score <= 9){
		$color = '#FF8000';
	}else if($score <= 10){
		$color = '#FF0000';
	}
	
	return $color;
}
/*
* Función encargada de imprimir el gráfico de resultados.
* Parámetros recibidos:
*	- httpResultsNumber: número de resultados HTTP
*	- httpsResultsNumber: número de resultados HTTPs
*	- sshResultsNumber: número de resultados SSH
*	- ftpResultsNumber: número de resultados FTP
*/
function graficoResultados($httpResultsNumber,$httpsResultsNumber,$sshResultsNumber,$ftpResultsNumber){
		echo '<script type="text/javascript">
      google.charts.load("current", {"packages":["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn("string", "Interfaz");
        data.addColumn("number", "Bytes");
        data.addRows([';
	
	//Coger datos interfaces
	echo '["HTTP", '.$httpResultsNumber.'],';
	echo '["HTTPs", '.$httpsResultsNumber.'],';
	echo '["SSH", '.$sshResultsNumber.'],';
	echo '["FTP", '.$ftpResultsNumber.']';
	
	echo']);
        var options = {"title":"Número de resultados",
                       "width":400,
                       "height":300};
        var chart = new google.visualization.PieChart(document.getElementById("grafico_interfacesRX"));
        chart.draw(data, options);
      }
    </script>';
}
/*
* Función encargada de devolver el punto de entrada 
* al ES. Lo obtiene de variable de entorno
*/
function getESHost(){
	$host = array();
	if(getenv('ES_HOST') !== null and !empty(getenv('ES_HOST')))
		array_push($host, getenv('ES_HOST'));
		// $host = getenv('ES_HOST');
	return $host;
}
?>
