<?php
/*
*	CÓDIGO ENCARGADO DE LA REPRESENTACIÓN 
*	DE LA PÁGINA PRINCIPAL
*/	
require_once('utiles.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Buscador</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
	<script type="text/javascript">
		function show_hide_row(row)
		{
		$("#"+row).toggle();
		}
	</script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<link rel="icon" href="darkiat_fav.png" type="image/gif" sizes="16x16">
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	
<?php
	pintarScriptReloj();
?>
	  
</head>
<body onload="startFechaHora()">
	

<div class="jumbotron text-center" style="margin-bottom:0">
  <img src="darkiat.png" class="img-fluid" alt="Darkiat" >
  <div id="reloj"></div> 
  <div id="fecha"></div> 
</div>

<?php
pintarMenu();
?>
<div class="container" style="margin-top:20px">
<?php
// Comprobar que vengan los parámetros de búsqueda y número de resultados
if(isset($_GET['s_id']) && !empty($_GET['s_id']) && isset($_GET['resultsNumber']) && !empty($_GET['resultsNumber'])){
	$exactMatch = False;
	if(isset($_GET['exactMatch']) && $_GET['exactMatch'] == 'on')
		$exactMatch = True;
	
	// Valor por defecto
	$resultsNumber = 10;
	try{
		// El usuario podría meter letras o algo inválido para romper la app, 
		$resultsNumber = intval($_GET['resultsNumber']);

		// O poner un número demasiado grande para fundir la DDBB
		if( $resultsNumber > 200 or $resultsNumber == 0) 
			$resultsNumber = 10;
	} catch (Exception $e) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	}
	
	// Comprobar si está buscando CVE o es cualquier otra cosa (host, software, key...)
	if(preg_match('/CVE-\d{4}-\d{4,}/', $_GET['s_id']) == 1){
		// echo '<h3>'.$_GET['s_id'].'</h3>';
		$cveDetails = buscarDetallesCVEs('cve_details', array($_GET['s_id']));
		
		
		echo '<div class="card"><h5 class="card-header"><a target="_blank" href="https://www.cvedetails.com/cve/'.$cveDetails['hits']['hits'][0]['_source']['cve_id'].'">'.$cveDetails['hits']['hits'][0]['_source']['cve_id'].'</a></h5><div class="card-body">';
		echo '<strong>CWE ID:</strong> '.$cveDetails['hits']['hits'][0]['_source']['cwe_id'];
		echo '<br><strong>CVSS score:</strong> <span style="background-color:'.getCVEColor($cveDetails['hits']['hits'][0]['_source']['cvss_score']).';">'.$cveDetails['hits']['hits'][0]['_source']['cvss_score'].'</span>';
		echo '<br><strong>Exploit count:</strong> '.$cveDetails['hits']['hits'][0]['_source']['exploit_count'];
		echo '<br><strong>Summary:</strong> '.$cveDetails['hits']['hits'][0]['_source']['summary'];
		echo '</div></div>';

		echo '<div class="row">
		<div class="col"><hr></div>
		</div>';
					
		
		// Obtener los software que tienen dicha CVE
		$cveResults = buscarCVESoftwareES('software_cve',$_GET['s_id']);
		$hostPintados = 0;
		
		// Una vez se dispone del software, se puede buscar en datos* para obtener los hosts
		foreach($cveResults['hits']['hits'] as $software){
			if ($hostPintados < $resultsNumber){
				$softwareTXT = $software['_source']['software'];
				
				// Si se busca por versión de software tiene que ser búsqueda exacta. Pasar TRUE
				$hostResults = buscarES('datos_*', $softwareTXT, $resultsNumber, True);
				
				foreach($hostResults['hits']['hits'] as $host){
					if ($hostPintados < $resultsNumber){
						printHost($host);
						$hostPintados = $hostPintados + 1;
					}else
						break;
				}
			}else
				break;
		}
	}else{// Si no busca CVE
		try {
			$httpResults = buscarES('datos_http', $_GET['s_id'],$resultsNumber, $exactMatch);
			$httpResultsNumber = $httpResults['hits']['total']['value'];
				
			$httpsResults = buscarES('datos_https', $_GET['s_id'],$resultsNumber, $exactMatch);
			$httpsResultsNumber = $httpsResults['hits']['total']['value'];
				
			$sshResults = buscarES('datos_ssh', $_GET['s_id'],$resultsNumber, $exactMatch);
			$sshResultsNumber = $sshResults['hits']['total']['value'];
					
			$ftpResults = buscarES('datos_ftp', $_GET['s_id'],$resultsNumber, $exactMatch);
			$ftpResultsNumber = $ftpResults['hits']['total']['value'];
			
			$totalPartialResults = $httpResultsNumber + $httpsResultsNumber + $sshResultsNumber + $ftpResultsNumber;
			
			echo '<h2 style="text-align:center">'.$totalPartialResults.' coincidencias</h2><div class="row">
			<div class="col-sm-6">
			  <div id="tabla"><table class="table table-hover">
		  <thead>
			<tr>
			  <th>Servicio</th>
			  <th>Resultados</th>
			</tr>
		  </thead>
		  <tbody>
			<tr>
			  <td>HTTP</td>
			  <td>'.$httpResultsNumber.'</td>
			</tr>
			<tr>
			  <td>HTTPs</td>
			  <td>'.$httpsResultsNumber.'</td>
			</tr>
			<tr>
			  <td>SSH</td>
			  <td>'.$sshResultsNumber.'</td>
			</tr>
			<tr>
			  <td>FTP</td>
			  <td>'.$ftpResultsNumber.'</td>
			</tr>
		  </tbody>
		</table>';
			
			echo '</div>
			  <hr class="d-sm-none">
			</div>
			<div class="col-sm-6">
			  <div id="grafico_interfacesRX"></div>
			</div></div>';
			graficoResultados($httpResultsNumber,$httpsResultsNumber,$sshResultsNumber,$ftpResultsNumber);
			// Solo buscar en el indice total si hay coincidencias parciales, sino no tiene sentido
			if($totalPartialResults > 0){
				$totalResults = buscarES('datos_*', $_GET['s_id'], $resultsNumber, $exactMatch);
				$totalResultsNumber = $totalResults['hits']['total']['value'];
				if($totalResultsNumber > $resultsNumber)
					echo '<h2 style="text-align:center">'.$resultsNumber.' resultados</h2>';
				else
					echo '<h2 style="text-align:center">'.$totalResultsNumber.' resultados</h2>';
				
				// Pintar cada host
				foreach($totalResults['hits']['hits'] as $host){
					printHost($host);
				}
			}

		} catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}

}else if(isset($_GET['host']) && !empty($_GET['host'])){
	// Si se ha hecho una búsqueda...

	// Hacer queries de ese host en los 4 índices y agruparlo todo
	$hostHTTP = buscarHostES('datos_http',$_GET['host']);
	$hostHTTPs = buscarHostES('datos_https',$_GET['host']);
	$hostSSH = buscarHostES('datos_ssh',$_GET['host']);
	$hostFTP = buscarHostES('datos_ftp',$_GET['host']);

	$output = '<div class="row">
	  <div class="col-sm-6">';
	
	$totalCVEs = array();
	$nombre = '';
	$categoria = '';
	$descripcion = '';
	$httpOutput = '';
	$httpsOutput = '';
	$sshOutput = '';
	$ftpOutput = '';
	if($hostHTTP['hits']['total']['value'] > 0){
		$output .= '<a href="#http_service" class="badge badge-secondary">HTTP</a>';
		$httpOutput = pintarHostHttp($hostHTTP);
		$httpCVEs = buscarSoftwareCVEES('software_cve',$hostHTTP['hits']['hits'][0]['_source']['server']);
		$nombre = $hostHTTP['hits']['hits'][0]['_source']['name'];
		$categoria = $hostHTTP['hits']['hits'][0]['_source']['category'];
		$descripcion = $hostHTTP['hits']['hits'][0]['_source']['description'];
		if(!empty($httpCVEs) && sizeof($httpCVEs) > 0 )
			array_push($totalCVEs, $httpCVEs['hits']['hits']);
	}
	if($hostHTTPs['hits']['total']['value'] > 0){
		$output .= '<a href="#https_service" class="badge badge-primary">HTTPS</a>';
		$httpsOutput = pintarHostHttps($hostHTTPs);
		$httpsCVEs = buscarSoftwareCVEES('software_cve',$hostHTTPs['hits']['hits'][0]['_source']['server']);
		if(empty($nombre))
			$nombre = $hostHTTPs['hits']['hits'][0]['_source']['name'];
		if(empty($categoria))
			$categoria = $hostHTTPs['hits']['hits'][0]['_source']['category'];
		if(empty($descripcion))
			$descripcion = $hostHTTPs['hits']['hits'][0]['_source']['description'];
		if(!empty($httpsCVEs) && sizeof($httpsCVEs) > 0 )
			array_push($totalCVEs, $httpsCVEs['hits']['hits']);
	}
	if($hostSSH['hits']['total']['value'] > 0){
		$output .= '<a href="#ssh_service" class="badge badge-info">SSH</a>';
		$sshOutput = pintarHostSsh($hostSSH);
		$sshCVEs = buscarSoftwareCVEES('software_cve',$hostSSH['hits']['hits'][0]['_source']['server']);
		if(empty($nombre))
			$nombre = $hostSSH['hits']['hits'][0]['_source']['name'];
		if(empty($categoria))
			$categoria = $hostSSH['hits']['hits'][0]['_source']['category'];
		if(empty($descripcion))
			$descripcion = $hostSSH['hits']['hits'][0]['_source']['description'];
		if(!empty($sshCVEs) && sizeof($sshCVEs) > 0 )
			array_push($totalCVEs, $sshCVEs['hits']['hits']);
	}
	if($hostFTP['hits']['total']['value'] > 0){
		$output .= '<a href="#ftp_service" class="badge badge-dark">FTP</a>';
		$ftpOutput = pintarHostFtp($hostFTP);
		$ftpCVEs = buscarSoftwareCVEES('software_cve',$hostFTP['hits']['hits'][0]['_source']['server']);
		if(empty($nombre))
			$nombre = $hostFTP['hits']['hits'][0]['_source']['name'];
		if(empty($categoria))
			$categoria = $hostFTP['hits']['hits'][0]['_source']['category'];
		if(empty($descripcion))
			$descripcion = $hostFTP['hits']['hits'][0]['_source']['description'];
		if(!empty($ftpCVEs) && sizeof($ftpCVEs) > 0 )
			array_push($totalCVEs, $ftpCVEs['hits']['hits']);
	}
	
	$output .= '<br><strong>Host:</strong> '.$_GET['host'];	
	if(!empty($nombre))
		$output .= '<br><strong>Nombre:</strong> '.$nombre;	
	if(!empty($categoria))
		$output .= '<br><strong>Categoria:</strong> '.$categoria;	
	if(!empty($descripcion))
		$output .= '<br><strong>Descripcion:</strong> '.$descripcion;	
	$output .= '<br>';
		
	$cvesBuscar = array();
	// Añadir cada CVE al array cvesBuscar
	foreach($totalCVEs as $serviceCVE){
		foreach($serviceCVE as $cve){
			array_push($cvesBuscar,$cve['_source']['cve_id']);
		}
	}

	// Eliminar duplicados
	$cvesBuscar = array_unique($cvesBuscar);
	
	// Obtener detalles de las CVEs
	$cveDetails = buscarDetallesCVEs('cve_details', $cvesBuscar);

	if(sizeof($cveDetails['hits']['hits']) > 0 ){
		// Cabecera tabla
		$output .= '<table class="table table-hover">
		   <thead>
			  <tr>
				 <th>CVE ID</th>
				 <th>CWE ID</th>
				 <th>CVSS score</th>
				 <th>Exploit count</th>
			  </tr>
		   </thead>
		   <tbody>';
		$counter = 0;
		// Pintar cada una de las CVE en formato tabla
		foreach($cveDetails['hits']['hits'] as $cve){
			$output .= '<tr onclick="show_hide_row(\'hidden_row'.$counter.'\')">';
			$output .= '<td><a target="_blank" href="https://www.cvedetails.com/cve/'.$cve['_source']['cve_id'].'">'.$cve['_source']['cve_id'].'</a></td>';
			$output .= '<td>'.$cve['_source']['cwe_id'].'</td>';

			$output .= '<td><span style="background-color:'.getCVEColor($cve['_source']['cvss_score']).';">'.$cve['_source']['cvss_score'].'</span></td>';
			$output .= '<td>'.$cve['_source']['exploit_count'].'</td>';
			$output .= '</tr>';
			$output .= '<tr id="hidden_row'.$counter.'" class="hidden_row" style="display: none;">';
			$output .= '<td colspan="4"><strong>Summary: </strong>'.$cve['_source']['summary'].'</td>';
			$output .= '</tr>';
			
			$counter = $counter + 1;
		}
		$output .= '</tbody>
		</table>';
	}else
		$output .= '<br>No se han encontrado CVEs asociadas';

	$output .= '</div><div class="col-sm-6">';
	  
	// Mostrar todos los datos
	print($output);
	print($httpOutput);
	print($httpsOutput);
	print($sshOutput);
	print($ftpOutput);

	echo '</div>
	</div>';

}else{
	// Si no hay parámetros o no los que se esperan, mostrar imagen inicial
	echo '<img src="portal3.png" class="img-fluid" alt="Example" >';
}
echo '
<div class="row">
    <div class="col"><hr/></div>
</div>	
</div>';
pintarPie();
?>

</body>
</html>
