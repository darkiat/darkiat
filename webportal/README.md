# PORTAL WEB
Este componente es totalmente opcional y está pensado para poder visualizar los datos y realizar búsquedas de forma rápida y sencilla. Puede sustituirse, hasta cierto punto, por Kibana; sin embargo la realización de búsquedas y actualización de gráficos en este último puede hacer que la experiencia de usuario sea demasiado lenta. Ese es el motivo principal por el que se ha decidido a desarrollar esta solución.

A continuación se incluye una captura, a modo de ejemplo, del buscador:
![captura portal](png/portal_1.png)


## CONTENIDOS

+ [Arquitectura](#arquitectura)
	+ [Parámetros de entrada](#parámetros-de-entrada)
		+ [Fichero de entrada](#fichero-de-entrada)
+ [Despliegue de la aplicación](#despliegue-de-la-aplicación)
+ [Ejemplo de uso](#ejemplo-de-uso)

# ARQUITECTURA
Como se muestra en la siguiente ilustración, el portal está compuesto por un contenedor que resulta intermediario entre el usuario y ElasticSearch, presentando al usuario una interfaz cómoda y sencilla sobre la que realizar búsquedas y representar los datos:
![portal arch](png/portal.png)

## PARÁMETROS DE ENTRADA

El portal web recibe un parámetro de entrada:

| Parámetro        | Descripción           | Valores posibles | Valor por defecto |
| ------------- |-------------|-------------|-------------|
| ES_HOST      | Punto de entrada al clúster/nodo de ElasticSearch | Dirección o IP. Ejemplos: micluster.midominio.com:9200, 172.12.25.66:9200 | No existe por defecto. Es **obligatorio** |


# DESPLIGUE DE LA APLICACIÓN

Al igual que las demás aplicaciones, se puede desplegar con: `docker-compose up` o `docker-compose up -d` si se desea ejecutar en segundo plano. Por defecto se mapea el puerto 80 del host al puerto 80 del contenedor, por lo que el portal web quedará accesible a través de la IP anfitrión. 

![despliegue_portal](png/despliegue_portal.png)

# EJEMPLO DE USO

Realizando una búsqueda se obtendrá una primera página que mostrará una tabla y un gráfico con el resumen de resultados y a continuación un listado con los mismos.
![portal_1](png/portal1.png)
Si se hace clic en alguno de los hosts se representa la información en dos columnas:
+	En la parte izquierda se muestran:
	+	unos enlaces a cada uno de los servicios que tiene activos el host
	+	datos del host: host, nombre, categoría, descripción. Estos datos los ha debido introducir el usuario al colector antes de realizar el escaneo. Se trata de datos opcionales, por lo que en muchos casos pueden no existir
	+	un listado en forma de tabla de las CVE y CWE que se han encontrado asociadas a las versiones de software que usan los servicios. Esta información se ha obtenido haciendo uso del **enriquecedor de vulnerabilidades**
		
+	En la parte de la derecha:
	+	los datos de cada servicio analizado: cabeceras, detalles, versiones de protocolos...
	
	
![portal host2](png/portal4_3.png)
