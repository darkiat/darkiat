![Logo Darkiat](png/darkiat.png)

**Copyright (C) 2020 [Daniel Moreno Belinchón](https://gitlab.com/d_moreno)**

License: GNU AGPLv3+

![License](png/agplv3.png)

# CONTENIDOS

+ [¿Qué es Darkiat?](#qué-es-darkiat)
+ [Componentes](#componentes)
+ [Arquitectura global](#arquitectura-global)
+ [Despliegue](#despliegue)
+ [Autor](AUTHORS.md)
+ [Licencia](LICENSE)
+ [Descargo de responsabilidad](#descargo-de-responsabilidad)
+ [Disclaimer](#Disclaimer)



# ¿QUÉ ES DARKIAT?

**DARKIAT** (Dark Investigation and Analysis Tool), es una herramienta para la investigación de servicios ocultos desplegados en la red Tor que, mediante la aplicación de procedimientos de inteligencia, sirve como herramienta de recopilación,integración, análisis e interpretación de información procedente de servicios alojados en la red Tor (The Onion Router).

# COMPONENTES

Se ha diseñado una aplicación compuesta por componentes con el fin de que el usuario decida cuáles y cómo necesita desplegarlos. Los componentes son los siguientes:

| Componente    | Propósito   | 
| ------------- |-------------|
| [Colector](tor_colector)      | Recopilar y almacenar información de servicios en red. Es capaz de enrutar las peticiones por la red normal y por la red Tor de forma simultánea |
| [Almacenamiento](almacenamiento)| Almacenar en ElasticSearch el contenido obtenido a través del colector.  |
| [Indexador](indexador)     | Almacenar datos procedentes del colector que no han sido indexados previamente |
| [Enriquecedor vulnerabilidades](cve)| Proporcionar información acerca de las vulnerabilidades y obsolescencia de las tecnologías descubiertas por la aplicación |
| [Portal](webportal)        | Agilizar y facilitar la explotación de la información por parte del Analista. Dispone de una interfaz web sobre la que se puede realizar cualquier tipo de consulta |

En función de las necesidades del analista en cada escenario podrá escoger qué componentes necesita y con qué modo de configuración. Para facilitar la decisión de cuáles desplegar se recomienda la visualización del [diagrama de despliegue](#despliegue).

# ARQUITECTURA GLOBAL

La unión e interacción entre los componentes de Darkiat se representa en la siguiente ilustración:

![Arquitectura Darkiat](png/darkiat-arquitectura.png)

# DESPLIEGUE

Como se ha indicado anteriormente, no es necesario desplegar todos y cada uno de los componentes de Darkiat, sino que el usuario podrá decidir cuáles necesita para satisfacer las necesidades de cada caso.

En el siguiente diagrama se representan un conjunto de situaciones que faciliten al usuario la tarea de decidir qué componenes necesita desplegar:

![Despliegue Darkiat](png/darkiat-despliegue.png)

Para desplegar cada uno de los componentes, se recomienda navegar a la documentación de los mismos:

+ [Colector](tor_colector) *
+ [Almacenamiento](almacenamiento)
+ [Indexador](indexador)
+ [Enriquecedor vulnerabilidades](cve)
+ [Portal](webportal)


\* El usuario puede hacer uso del automatismo que se ha preparado para desplegar el colector en la nube, en concreto en AWS. Más información [aquí](aws)


# DESCARGO DE RESPONSABILIDAD

El usuario asume todas las responsabilidades derivadas del uso de la información y herramientas aquí incluidas, incluyendo pero sin limitarse a todos aquellos actos, daños, perjuicios o requerimientos legales que pudiesen  ocasionarse por el uso de los mismos, quedando exento de toda responsabilidad el autor de la memoria.

Por lo tanto, **cualquier confianza que el lector/usuario deposite en dicho material es estrictamente bajo su propio riesgo**.

La herramienta desarrollada, Darkiat, se distribuye bajo la licencia AGPLv3+, sin perjuicio de posibles cambios a futuro. 

Para más información consultar [el apartado de licencia](LICENSE).

# DISCLAIMER

The user assumes all the responsibilities derived from the use of the information and tools included here, including but not limited to all those acts,  damages or legal requirements that may be caused by the use of them, being exempt of any responsability the autor of this memory.

Therefore, **any trust that the reader/user places in such material is strictly at their own risk**.

The application developed, Darkiat, is published under AGPLv3+ license, without prejudice to possible future changes. 

For more information check [license page](license).



