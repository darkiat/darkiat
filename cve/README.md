# ENRIQUECEDOR DE DATOS CON CVE
Este componente está pensado para, una vez se dispone de versiones de software usadas, enriquecer los datos de ElasticSearch con información adicional sobre las [CVE (Common Vulnerabilities and Exposures)](https://cve.mitre.org/) que pueden tener determinados servicios.

A continuación se recoge un resumen de la catalogación de CVE realizado por [cvedetails.com](https://www.cvedetails.com/). Como se puede observar en mayo de 2020 existen un total de 123.454 vulnerabilidades catalogadas de las que un 13.10% tienen una puntuación comprendida entre 9 y 10.
![cve_distribution](png/cve_distribution.png)


## CONTENIDOS

+ [Arquitectura](#arquitectura)
	+ [Parámetros de entrada](#parámetros-de-entrada)
		+ [Fichero de entrada](#fichero-de-entrada)
+ [Despliegue de la aplicación](#despliegue-de-la-aplicación)

# ARQUITECTURA
Como se muestra en la siguiente ilustración, está compuesta de un único componente que reúne todas las dependencias necesarias para descargar detalles de CVE asociadas a software e indexarlos en ElasticSearch:
![enriquecedor arch](png/enriquecedor.png)

## PARÁMETROS DE ENTRADA

El enriquecedor recibe tres parámetro de entrada:

| Parámetro        | Descripción           | Valores posibles | Valor por defecto |
| ------------- |-------------|-------------|-------------|
| INPUT_FILE    | Fichero de entrada que contiene las versiones de software y sus enlaces a cvedetails. El fichero debe existir dentro de la carpeta [codigo](codigo) [1]| myfile.txt | **VALOR OBLIGATORIO** |
| ES_HOST      | Punto de entrada al clúster/nodo de ElasticSearch | Dirección o IP. Ejemplos: micluster.midominio.com, 172.12.25.66 |localhost |
| USER_AGENT      | Agente de usuario con el que se desean realizar las consultas. | Agente de usuario en cadena de texto | *Vacío* |

### FICHERO DE ENTRADA
El fichero pasado como parámetro *INPUT_FILE* debe existir dentro de la carpeta [codigo](codigo) y debe atender al siguiente formato:
```
<versión de software>;<url en cve-details>
```

Ejemplo:
```
Apache/2.4.25;https://www.cvedetails.com/vulnerability-list/vendor_id-45/product_id-66/version_id-218176/Apache-Http-Server-2.4.25.html
Apache/2.4.27;https://www.cvedetails.com/vulnerability-list/vendor_id-45/product_id-66/version_id-224607/Apache-Http-Server-2.4.27.html
```

# DESPLIGUE DE LA APLICACIÓN

Al igual que las demás aplicaciones, se puede desplegar con: `docker-compose up` o `docker-compose up -d` si se desea ejecutar en segundo plano.

Desde Kibana se puede comprobar que los datos han sido indexados correctamente en ElasticSearch:
![indices cve](png/cve_indices.png)
