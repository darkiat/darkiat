from elasticsearch import Elasticsearch
import json 
import sys
import re
import os
import requests

# Devuelve el número de veces que esa CVE está guardada en ES
def search_cve_es(es,index,cve_id):
	hits = 0
	# Construimos la búsqueda
	search_param = {
		'query': {
			'match': {
				'cve_id.keyword': cve_id
			}
		}
	}
	try:
		# Hacer la búsqueda en ElasticSearch
		resultado = es.search(index='cve_details', size=1,body=search_param)
		encoded_json = json.dumps(resultado)
		decoded_json = json.loads(encoded_json)

		took = decoded_json['took']
		timed_out = decoded_json['timed_out']
		hits_total = decoded_json['hits']['total']['value']
		hits = decoded_json['hits']['total']['value']
		
		# print(decoded_json)
		# print(hits)

	except Exception as e:
		print("Fallo en la búsqueda en ES ",str(e))
		
	return hits		

def putES(es,index,data):
	resultado = False
	try:
		res = es.index(index=index,body=data)
		if (res['result'] == 'created'):
			resultado = True
	except Exception as e:
		print("Error de indexación")
	return resultado


def find_cve(data, es, userAgent):
	print(data)
	dataSplitted = data.split(';')
	if(len(dataSplitted) == 2):
		url = dataSplitted[1]
		pattern = 'https://www.cvedetails.com/vulnerability-list/vendor_id-(?P<vendor>.*?)/product_id-(?P<product>.*?)/version_id-(?P<version>.*?)/.*'
		matches = re.search(pattern, url)
		if matches:
			vendor_id = matches.group('vendor')
			product_id = matches.group('product')
			version_id = matches.group('version')
			if version_id:
				# Esta es la URL que tiene los JSON
				url = "http://www.cvedetails.com/json-feed.php?numrows=30&vendor_id="+vendor_id+"&product_id="+product_id+"&version_id="+version_id
			else:
				url = "http://www.cvedetails.com/json-feed.php?numrows=30&vendor_id="+vendor_id+"&product_id="+product_id
			
			print("\t"+url)
			
			r = requests.get(
				url,
				headers={"User-Agent": userAgent},
			)

			if r.status_code != 200:
				print("HTTP Error occured. Code {} returned".format(r.status_code))
			else:
				# print(r.json())
				
				cveResponse = r.json()
				print("\tCVEs:",len(cveResponse))
				for cve in cveResponse:
					# Si el CVE no está guardado ya... se guarda
					if search_cve_es(es,'cve_details',cve['cve_id']) == 0:
						e1 = {
							'cve_id': cve['cve_id'],
							'cwe_id': cve['cwe_id'],
							'summary': cve['summary'],
							'cvss_score': cve['cvss_score'],
							'exploit_count': cve['exploit_count'],
							'publish_date': cve['publish_date'],
							'update_date': cve['update_date'],
							'url': cve['url'],
						}
						if not putES(es,"cve_details",e1):
							print("Error de indexación",cve['cve_id'])
					else:
						print("El cve",cve['cve_id'],"ya existe, NO se guardará de nuevo")

					s1 = {
						'software': dataSplitted[0],
						'server': dataSplitted[0],
						'cve_id': cve['cve_id'],
					}
					if not putES(es,"software_cve",s1):
						print("Error de indexación",data)

				print(40*"-")

# Obteniendo variables de entorno
inputFile = os.getenv('INPUT_FILE')	
esHost = os.getenv('ES_HOST','localhost')	
userAgent = os.getenv('USER_AGENT','')	
if(inputFile and esHost):
	inputFile = open(inputFile,'r')
	es = Elasticsearch(hosts=esHost)
	# Si no responde el cluster, salir del programa indicando el motivo
	if not es.ping():
		print("ElasticSeach NO responde en el host "+esHost)
		sys.exit(1)

	for software in inputFile.read().split('\n'):
		# Si la línea no está vacía
		if len(software) != 0:
			print(software)
				
			find_cve(software,es,userAgent)

	# Cerrar fichero de entrada
	inputFile.close()
else:
	print("Debe pasar dos variables de entorno:\n\tINPUT_FILE\n\tES_HOST")
	sys.exit(1)