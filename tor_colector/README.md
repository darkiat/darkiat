# TOR COLECTOR
Esta parte es el núcleo central de la aplicación y se encarga de llevar a cabo los escaneos de servicios.

Está diseñada para navegar por la surface web y por la deep web, de forma que dependiendo del tipo de servicio a escanear saldrá por una u otra. 

## CONTENIDOS

+ [Arquitectura](#arquitectura)
	+ [Colector](#colector)
		+ [Parámetros de entrada](#parámetros-de-entrada)
			+ [Fichero con hosts](#fichero-con-hosts)
			+ [Argumentos](#argumentos)
	+ [Tor router](#tor-router)
+ [Despliegue de la aplicación](#despliegue-de-la-aplicación)
	+ [Ejemplo de uso](#ejemplo-de-uso)

# ARQUITECTURA
Como se muestra en la siguiente ilustración, está compuesta de dos componentes principales, desplegados en contenedores:
![tor colector arch](png/colector.png)

Como se puede observar en la ilustración anterior, el componente colector tiene dos formas de guardar la información:
+	**En sistema de ficheros**: pensado para aquellos entornos ligeros en los que no se dispone o no se desea desplegar estructuras de almacenamiento como ElasticSearch. En este modo de funcionamiento el colector guardará todo en formato texto, dentro de la carpeta ***resultados***, siguiendo una estructura predefinida y permitiendo que dichos datos puedan ser almacenados a posteriori haciendo uso del componente *indexador* que se detallará a continuación.

+	**En ElasticSearch**: pensado para aquellos entornos en los que se dispone de conexión con un clúster/nodo de ElasticSearch. Permite el almacenamiento directo de la información y en este caso el *colector* es el encargado de indexar la información automáticamente, sin necesidad de hacer ningún tipo de operación después.

A continuación, en el apartado [parámetros de entrada](#parámetros-de-entrada) se define el modo de configuración de la aplicación.

## COLECTOR
Es el componente encargado de realizar las operaciones de escaneo y fingerprinting de servicios. Como se muestra en la siguiente ilustración puede navegar directamente por la *surface web* o hacerlo por la *deep web* a través del contenedor *TOR ROUTER*
![tor colector arch](png/colector2.png)

Este componente espera recibir dos ficheros con parámetros:
### PARÁMETROS DE ENTRADA

#### **Fichero con hosts**: 
Se trata del fichero *[input_file](codigo/input_file)* que se encuentra dentro de la carpeta codigo. Es un fichero en formato CSV que debe contener una entrada por cada servicio que se desea escanear. Los campos son los siguientes:
`<host>;<servicio>;<puerto>;<nombre>;<descripción>;<categoría>`

| Parámetro     | Descripción           | Contenido |
| ------------- |-------------|-------------|
| host          | Dirección del host. Puede ser una IP o una dirección (normal u onion) | **OBLIGATORIO** |
| servicio      | Uno de los siguientes valores: ssh, http, https, ftp | **OBLIGATORIO** |
| puerto        | Puerto en el que está expuesto el servicio | **OBLIGATORIO** |
| nombre        | Nombre del servicio | OPCIONAL |
| descripción   | Descripción del servicio | OPCIONAL |
| categoría     | Categoría del servicio     | OPCIONAL |

**Todos los campos son obligatorios**, aunque pueden estar vacíos. 
A continuación se recoge un ejemplo de fichero de entrada en el que se recogen cuatro servicios:
```
10.172.25.33;ssh;22;Servidor central;Servicio SSH interempresas;SSH empresarial;Inventario empresa
10.172.25.33;http;8080;Servidor central;Servicio web central;Web empresarial;Inventario empresa
<dirección-onion>;https;443;Onion forum;Foro de asuntos tecnológicos;Tecnología;Descubrimiento propio
10.172.25.85;ftp;21;;;;
```

#### **Argumentos**: 
El componente colector está diseñado para funcionar de diferentes formas en función de las necesidades del usuario. El modo de funcionamiento se escoge a través del fichero *[variables.env](variables.env)* en el que se definen variables de entorno para los contenedores Docker.

| Parámetro        | Descripción           | Valores posibles | Valor por defecto |
| ------------- |-------------|-------------|-------------|
| SAVE_FS_DATA      | Permite decidir si se desean guardar los datos en sistema de ficheros o no | TRUE o FALSE | FALSE |
| INDEX_DATA      | Permite decidir si se desean indexar los datos en ElasticSearch de ficheros o no | TRUE o FALSE | TRUE |
| ES_HOST      | Punto de entrada al clúster/nodo de ElasticSearch | Dirección o IP. Ejemplos: micluster.midominio.com, 172.12.25.66 |localhost |
| INPUT_FILE      | Nombre del fichero que recoge los servicios a analizar. [Véase el apartado anterior](#fichero-con-hosts). El fichero debe estar dentro de la carpeta [codigo](codigo), que se monta en el sistema de ficheros del contenedor| Nombre de fichero, que exista y cumpla con el formato descrito en el [apartado anterior](#fichero-con-hosts) | input_file |
| ONION_ONLY      | Booleano que permite decidir si se desean escanear **sólo direcciones onion válidas**. Así, se descartará cualquier host que presente una dirección inválida [1]| TRUE o FALSE | TRUE |
| MAX_WORKERS      | El colector está diseñado para trabajar concurrentemente, es decir, para escanear determinados host de forma simultánea, por lo que carece de interés desplegar *n* réplicas de *colectores* a la vez. El usuario puede establecer, en función de sus recursos y su criterio, el número de servicios que desea escanear de forma simultánea ajustando este parámtero. Para más información [véase ThreadPoolExecutor](https://docs.python.org/3/library/concurrent.futures.html) | Número entero | 5 |
| ONION_REFERENCE      | Por el modo de funcionamiento de las instancias TOR, es posible que los circuitos tarden un cierto tiempo en establecerse, sobre todo los primeros. Para evitar que los primeros servicios a escanear pudieran dar timeout y obteniendo resultados erróneos se permite realizar una verificación de un servicio *onion* que se conozca que está funcionando de forma previa al escaneo. Se intentará hasta un máximo de 12 veces | Dirección onion válida | *Por defecto desactivado. Puede omitir este parámetro* |
| USER_AGENT      | Agente de usuario con el que se desean realizar las consultas. | Agente de usuario en cadena de texto | *Vacío* |

[1] Las direcciones onion se validan de acuerdo a las especificaciones oficiales de [Tor Project](https://www.torproject.org/), accesibles en:
+ [Especificación del protocolo v2](https://gitweb.torproject.org/torspec.git/tree/rend-spec-v2.txt)
+ [Especificación del protocolo v3](https://gitweb.torproject.org/torspec.git/tree/rend-spec-v3.txt)

A continuación se muestra un ejemplo de fichero [variables.env](variables.env)
```
SAVE_FS_DATA=TRUE
INDEX_DATA=FALSE
ES_HOST=192.168.20.224
INPUT_FILE=input_file
ONION_ONLY=FALSE
MAX_WORKERS=10
ONION_REFERENCE=****************.onion
USER_AGENT=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.2 Safari/605.1.15
```
## TOR ROUTER

Este contenedor se encarga de enrutar las peticiones provenientes del *colector* a través de la red TOR. Está configurado para ello y por razones de seguridad solo enruta el tráfico proveniente de dicho colector, gracias al aislamiento proporcionado por la red Docker creada para este propósito.

Principalmente, este gateway a TOR está ejecutando tres servicios:
1.	Servicio de resolución de nombres: cuando el *colector* lanza una petición DNS preguntando por la dirección IP de un dominio onion, este le contestará con una IP del rango `10.66.0.0/255.255.0.0` El *colector* está configurado adecuadamente (a nivel de rutas y de red) para enviar todo el tráfico cuyo destino sea dicho rango de direcciones al contenedor TOR.
2.	Servicio de transporte TOR. Se trata del servicio que realmente permite la navegación a través de la red TOR, siendo capaz de enrutar cualquier petición proveniente del *colector*
3.	Servicio de control, por si se desea consultar el estado de la instancia TOR, circuitos, conexiones... en cualquier momento. Para eso pueden resultar útiles herramientas como [nyx](https://nyx.torproject.org/)


# DESPLIEGUE DE LA APLICACIÓN
En este caso se van a barajar diferentes formas de despliegue de la aplicación:
+ Si se desean levantar ambos contenedores de forma interactiva, puede hacerse con el comando `docker-compose up`
+ Si se desean levantar ambos contenedores de forma no interactiva, puede hacerse con el comando `docker-compose up -d`

Con cualquiera de estas dos opciones citadas anteriormente el *colector* finalizará su ejecución y el contenedor parará cuando acabe de escanear todos los hosts, pero la instancia TOR continuará activa hasta que se desactive manualmente. Si se prefiere que el contenedor TOR finalice su ejecución cuando acabe el *colector* se puede lanzar con el siguiente comando: `docker-compose up --abort-on-container-exit` 

El *colector* depende del contenedor TOR, lo que significa que hasta que el segundo no arranque no comenzará el primero. Esto se pondrá de manifiesto a continuación en los mensajes de log.

## EJEMPLO DE USO
A continuación se muestra una captura de ejemplo del arranque de la aplicación, en la que se puede observar:
+	**Logs** de la instancia **TOR**. Ponen de manifiesto los servicios que se han activado, en qué direcciones, puertos... Muestran una serie de avertencias, que en este caso no aplican por tratarse de un entorno acotado, privado y aislado a través de la red Docker.
+	**Logs** de arranque del ***colector***. Se muestra:
	+ todos los parámetros que definirán el modo de funcionamiento de la aplicación. 
	+ las comprobaciones, si se han activado, en la red onion, que permiten saber cuando la instancia TOR está lista para trabajar
	+ el número de workers que, concurrentemente, se ejecutarán
	+ el número de escaneos que se van a realizar, clasificados por servicio

![arranque 1](png/arranque1_git.png)

Posteriormente se muestra una entrada por cada servicio que se ha escaneado, como sigue a continuación:
![arranque 2](png/arranque2_git.png)

Dado que esta ejecución se ha configurado para guardar datos en sistema de ficheros y en ElasticSearch, se va a comprobar que se ha almacenado en ambos sitios correctamente:
En sistema de ficheros se observa la siguiente estructura:
![resultados_fs](png/resultados_fs.png)

Esta carpeta ***resultados*** será la que hay que guardar si se desean indexar los datos posteriormente. Para ello, se ha creado el componente indexador.

**Nota:** el fichero *logs* recoge una entrada por cada servicio escaneado, guardando información adicional como el número de bytes de respuesta y la fecha en la que se ha obtenido dicha respuesta. Ambos campos serán útiles para indexar a posteriori si se decide.

Desde Kibana se puede comprobar que los datos han sido indexados correctamente en ElasticSearch:
![resultados_es](png/resultados_es.png)
