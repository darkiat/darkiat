﻿from utils import utils
import subprocess
import pexpect
import re
import os
from OpenSSL import crypto

class escaner:

	# Devolver:
	# protocolo,dirección,puerto,bytesAlmacenados,fechaBusqueda
	def escaner_http(site, es, saveFSDataBool,indexDataBool):
		print("http",site.get_host(),site.get_port())
		# print("curl --max-time 30 -s -A \""+site.get_user_agent()+"\" -D "+site.get_results_path()+" -o /dev/null "+site.get_host()+":"+site.get_port())
		subprocess.getoutput("curl --max-time 30 -s -A \""+site.get_user_agent()+"\" -D "+site.get_results_path()+" -o /dev/null "+site.get_host()+":"+site.get_port())
		dataSize = os.stat(site.get_results_path()).st_size
		time = utils.getTime()
		
		utils.write_on_file(site, site.get_host()+";"+site.get_service().upper()+";"+str(site.get_port())+";"+site.get_name()+";"+site.get_description()+";"+site.get_category()+";"+site.get_source()+";"+str(dataSize)+";"+time+"\n")
		
		
		# Indexar si hay datos y procede
		if(dataSize > 0 and indexDataBool):
			dataFile = open(site.get_results_path(),'r')
			headerData = dataFile.read()
			dataFile.close()
			
			# Get server data
			server = re.findall("[Ss]erver: (.*)",headerData.strip())
			
			e1 = {
				'date': time,
				'host': site.get_host(),
				'service':'HTTP',
				'port': site.get_port(),
				'headers': headerData,
				'server': server,
				'name': site.get_name(),
				'description': site.get_description(),
				'category': site.get_category(),
				'source': site.get_source(),
			}
			if not utils.putES(es,"datos_http",e1):
				print("Error de indexación",site.get_host())
		
		if not saveFSDataBool:
			subprocess.getoutput("rm -r "+site.get_results_path())
	
	# Devolver:
	# protocolo,dirección,puerto,bytesAlmacenados,fechaBusqueda
	def escaner_https(site,es, saveFSDataBool,indexDataBool):
		print("https",site.get_host(),site.get_port())
		subprocess.getoutput("curl --max-time 30 -s -A \""+site.get_user_agent()+"\" -D "+site.get_results_path()+"-H -o /dev/null -k https://"+site.get_host()+":"+site.get_port())
		dataSize = 0
		# Que existe el fichero y que tenga datos, significa que el servicio está arriba
		if( os.path.exists(site.get_results_path()+"-H") and os.stat(site.get_results_path()+"-H").st_size > 0 ):
			subprocess.getoutput("openssl s_client -showcerts -connect "+site.get_host()+":"+str(site.get_port())+" < /dev/null 2> /dev/null | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' >"+site.get_results_path()+"-C")
			dataSize = os.stat(site.get_results_path()+"-H").st_size + os.stat(site.get_results_path()+"-C").st_size
		time = utils.getTime()
		
		utils.write_on_file(site, site.get_host()+";"+site.get_service().upper()+";"+str(site.get_port())+";"+site.get_name()+";"+site.get_description()+";"+site.get_category()+";"+site.get_source()+";"+str(dataSize)+";"+time+"\n")
		
		# Solo se indexa si hay datos
		if(dataSize > 0 and indexDataBool):
			headerDataFile = open(site.get_results_path()+"-H",'r')
			headerData = headerDataFile.read()
			headerDataFile.close()
			
			certDataFile = open(site.get_results_path()+"-C",'r')
			cert_data = certDataFile.read()
			certDataFile.close()
			
			# Get server data
			server = re.findall("[Ss]erver: (.*)",headerData.strip())

			# Obtener datos del certificado		
			cert = crypto.load_certificate(crypto.FILETYPE_PEM, cert_data)
			cert_dumped = crypto.dump_certificate(crypto.FILETYPE_TEXT, cert)
			e1 = {
				'date': time,
				'host': site.get_host(),
				'service':'HTTPS',
				'port': site.get_port(),
				'headers': headerData,
				'server': server,
				'certificate': cert_data,
				'certificate_txt': cert_dumped.decode(),
				'name': site.get_name(),
				'description': site.get_description(),
				'category': site.get_category(),
				'source': site.get_source(),
			}
			
			if not utils.putES(es,"datos_https",e1):
				print("Error de indexación",site.get_host())
				
		if not saveFSDataBool:
			subprocess.getoutput("rm -r "+site.get_results_path()+"-H "+site.get_results_path()+"-C")		

	def escaner_ssh(site,es, saveFSDataBool,indexDataBool):
		print("ssh",site.get_host(),site.get_port())
		
		# Se lanza la petición ssh con pexpect
		try:
			child = pexpect.spawn ("ssh -vv "+site.get_host()+" -p "+site.get_port()+" -o FingerprintHash=md5")
			child.expect ("debug1: Server host key:.*")
			child.sendline ('\x03')
			salida = child.before + child.after
			salida = salida.decode()

			# Bulk mode
			matchRemoteProtocol = re.findall("debug1: Remote protocol.*",salida)
			matchKEXAlgo = re.findall("debug2: KEX algorithms:.*",salida)
			matchHostKeyAlgo = re.findall("debug2: host key algorithms:.*",salida)
			matchCiphers = re.findall("debug2: ciphers stoc:.*",salida)
			matchMACs = re.findall("debug2: MACs stoc:.*",salida)
			matchCompression = re.findall("debug2: compression stoc:.*",salida)
			matchHostKey = re.findall("debug1: Server host key:.*",salida)
			
			# Comprobar que existen los campos esperados
			if(len(matchRemoteProtocol) == 1 and len(matchKEXAlgo) == 2 and len(matchHostKeyAlgo) == 2 and len(matchCiphers) == 2 and len(matchMACs) == 2 and len(matchCompression) == 2 and len(matchHostKey) == 1):
				# Construir la cadena con los datos para el fichero
				# Nótese que de algunos campos se coge el primer resultado [0] y de otros el segundo [1], esto es para descartar los datos del cliente ssh
				sshData = matchRemoteProtocol[0].strip() + "\n" + matchKEXAlgo[1].strip() + "\n" + matchHostKeyAlgo[1].strip() + "\n" + matchCiphers[1].strip() + "\n" + matchMACs[1].strip() + "\n" + matchCompression[1].strip() + "\n" + matchHostKey[0].strip() + "\n"
				
				# Escribir los datos en el fichero
				if saveFSDataBool:
					ficheroSalida = open(site.get_results_path(),'w')
					ficheroSalida.write(sshData)
					ficheroSalida.flush()
					ficheroSalida.close()

				# Guardar log de datos obtenidos
				dataSize = len(sshData)
				time = utils.getTime()
				
				utils.write_on_file(site, site.get_host()+";"+site.get_service().upper()+";"+str(site.get_port())+";"+site.get_name()+";"+site.get_description()+";"+site.get_category()+";"+site.get_source()+";"+str(dataSize)+";"+time+"\n")
				
				# Si se desea indexar...
				if indexDataBool:
					# Patrones para encontrar datos relevantes
					sshVersionPattern	= "Remote protocol version (.*?), remote software version (.*)"
					kexAlgoPattern	= "debug2: KEX algorithms: (.*)"
					hostKeyAlgoPattern	= "debug2: host key algorithms: (.*)"
					ciphersPattern	= "debug2: ciphers stoc: (.*)"
					macsPattern	= "debug2: MACs stoc: (.*)"
					compressionPattern	= "debug2: compression stoc: (.*)"
					hostKeyPattern = "debug1: Server host key: (.*) (.*)"
					
					# Buscar todos los patrones 
					sshVersionResults = re.search(sshVersionPattern, matchRemoteProtocol[0].strip())
					kexAlgoResults = re.search(kexAlgoPattern, matchKEXAlgo[1].strip())
					keyAlgoResults = re.search(hostKeyAlgoPattern, matchHostKeyAlgo[1].strip())
					ciphersResults = re.search(ciphersPattern, matchCiphers[1].strip())
					macsResults = re.search(macsPattern, matchMACs[1].strip())
					compressionResults = re.search(compressionPattern, matchCompression[1].strip())
					hostKeyTypeResults = re.search(hostKeyPattern, matchHostKey[0].strip())
					
					
					# Construir el JSON cabecera, se irá completando a continuación
					e1 = {
						'date': utils.getTime(),
						'host': site.get_host(),
						'service':'SSH',
						'port': site.get_port(),
						'name': site.get_name(),
						'description': site.get_description(),
						'category': site.get_category(),
						'source': site.get_source(),
					}

					# Si se ha encontrado la versión del protocolo y software, añadirlo para guardarlo en ES
					if sshVersionResults and len(sshVersionResults.groups()) == 2:
						sshVersion = sshVersionResults.groups()[0]
						sshSoftwareVersion = sshVersionResults.groups()[1]
						e1['ssh_version'] = sshVersion
						e1['ssh_software_version'] = sshSoftwareVersion
						e1['server'] = sshSoftwareVersion

					# Si se han encontrado datos de kex, añadirlos para guardarlo en ES
					if kexAlgoResults and len(kexAlgoResults.groups()) == 1:
						kexAlgo = kexAlgoResults.groups()[0]
						e1['kex_algo'] = kexAlgo
					
					# Si se han encontrado datos de algoritmos de claves, añadirlo para guardarlo en ES
					if keyAlgoResults and len(keyAlgoResults.groups()) == 1:
						keyAlgo = keyAlgoResults.groups()[0]
						e1['key_algo'] = keyAlgo

					# Si se han encontrado datos de algoritmos de cifrado, añadirlo para guardarlo en ES
					if ciphersResults and len(ciphersResults.groups()) == 1:
						ciphers = ciphersResults.groups()[0]
						# print(ciphers)
						e1['ciphers'] = ciphers

					# Si se han encontrado datos de algoritmos de mac, añadirlo para guardarlo en ES
					if macsResults and len(macsResults.groups()) == 1:
						macs = macsResults.groups()[0]
						# print(macs)
						e1['macs'] = macs

					# Si se han encontrado datos de compresión, añadirlo para guardarlo en ES
					if compressionResults and len(compressionResults.groups()) == 1:
						compression = compressionResults.groups()[0]
						# print(compression)
						e1['compression'] = compression
						
					# Si se han encontrado datos del tipo de clave y el fingerprint, añadirlo para guardarlo en ES
					if hostKeyTypeResults and len(hostKeyTypeResults.groups()) == 2:
						hostKeyType = hostKeyTypeResults.groups()[0]
						hostKeyFingerprint = hostKeyTypeResults.groups()[1]
						e1['host_key_type'] = hostKeyType
						e1['host_key_fingerprint'] = hostKeyFingerprint

					if not utils.putES(es,"datos_ssh",e1):
						print("Error de indexación",site.get_host())
			else:	
				print(utils.FAIL+"ERROR"+utils.ENDC)
		except Exception as e:
			# Si hay error hay que guardar log de que no se han conseguido datos
			print(utils.FAIL+"  SSH ERROR"+utils.ENDC)
			dataSize = 0
			time = utils.getTime()
			utils.write_on_file(site, site.get_host()+";"+site.get_service().upper()+";"+str(site.get_port())+";"+site.get_name()+";"+site.get_description()+";"+site.get_category()+";"+site.get_source()+";"+str(dataSize)+";"+time+"\n")


	# Devolver:
	# protocolo,dirección,puerto,bytesAlmacenados,fechaBusqueda
	def escaner_ftp(site,es,saveFSDataBool,indexDataBool):
		print("ftp",site.get_host(),site.get_port())
		subprocess.getoutput("curl  --max-time 30 -s -A \""+site.get_user_agent()+"\" -D "+site.get_results_path()+" -o /dev/null ftp://"+site.get_host()+":"+site.get_port())
		dataSize = os.stat(site.get_results_path()).st_size
		time = utils.getTime()
		
		utils.write_on_file(site, site.get_host()+";"+site.get_service().upper()+";"+str(site.get_port())+";"+site.get_name()+";"+site.get_description()+";"+site.get_category()+";"+site.get_source()+";"+str(dataSize)+";"+time+"\n")
		
		# Solo se indexa si hay datos
		if(dataSize > 0 and indexDataBool):
			dataFile = open(site.get_results_path(),'r')
			headerData = dataFile.read()
			dataFile.close()
			
			e1 = {
				'date': time,
				'host': site.get_host(),
				'service':'FTP',
				'port': site.get_port(),
				'headers': headerData,
				'name': site.get_name(),
				'description': site.get_description(),
				'category': site.get_category(),
				'source': site.get_source(),
			}
			
			if not utils.putES(es,"datos_ftp",e1):
				print("Error de indexación",site.get_host())

		if not saveFSDataBool:
			subprocess.getoutput("rm -r "+site.get_results_path())
			
			
	# Devolver:
	# protocolo,dirección,puerto,bytesAlmacenados,fechaBusqueda
	def test_http_connection(address, port):
		return subprocess.getoutput("curl --max-time 10 -s -I "+address+":"+str(port))
