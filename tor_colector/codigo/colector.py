﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
from elasticsearch import Elasticsearch
import time
from escaner import escaner
from utils import utils
from scan_site import site
from concurrent.futures import ThreadPoolExecutor

def main():
	# Predefinido, coherente con el docker-compose
	resultsPath = '/results'
	
	startTime = time.time()
	print(utils.OKGREEN+"[START] "+str(startTime)+utils.ENDC)
	
	# Obteniendo variables de entorno
	saveFSData = os.getenv('SAVE_FS_DATA','FALSE')
	indexData = os.getenv('INDEX_DATA','TRUE')
	esHost = os.getenv('ES_HOST','localhost')
	inputFileName = os.getenv('INPUT_FILE','input_file')
	onionOnly = os.getenv('ONION_ONLY','TRUE')
	maxWorkers = os.getenv('MAX_WORKERS','5')
	onionReference = os.getenv('ONION_REFERENCE')
	userAgent = os.getenv('USER_AGENT', '')
	
	# Imprimir modo de funcionamiento
	print("-"*30)
	print('SAVE_FS_DATA:',saveFSData)
	print('INDEX_DATA:',indexData)
	print('ES_HOST:',esHost)
	print('INPUT_FILE:',inputFileName)
	print('ONION_ONLY:',onionOnly)
	print('ONION_REFERENCE:',onionReference)
	print('USER_AGENT:',userAgent)
	print("-"*30)

	# Evaluación de parámetros de entrada
	indexDataBool = False
	saveFSDataBool = False
	es = None
	# Si se desea indexar en ElasticSearch inicializar conexión
	if(indexData == 'TRUE'):
		indexDataBool = True
		es = Elasticsearch(hosts=esHost)
		# Si no responde el cluster, salir del programa indicando el motivo
		if not es.ping():
			print(utils.FAIL+"ElasticSeach NO responde en el host "+esHost)
			sys.exit(1)
			
	if(saveFSData == 'TRUE'):
		saveFSDataBool = True
	elif not indexDataBool:
		# En caso de que no se haya seleccionado el almacenamiento en FS ni ES, salir
		print(utils.FAIL+"No tiene sentido usar la APP si no se van a guardar datos en ningún sitio")
		sys.exit(1)

	# Comprobar que el número de workers metido por el usuario es un número		
	try:
		maxWorkersNum = int(maxWorkers)
	except ValueError:
		maxWorkersNum = 5

	# Si se quiere comprobar una conexión http de referencia antes de arrancar
	if onionReference:
		if utils.verify_host(onionReference):
			i = 0
			torWorking = False
			while i < 12 and not torWorking:
				print("Check",str(i+1)+"-"+"12",onionReference)
				resultsHTTP = escaner.test_http_connection(onionReference,80)
				if resultsHTTP and len(resultsHTTP) > 0:
					torWorking = True
				i += 1
			if not torWorking:
				print(utils.FAIL+"La instancia TOR no está funcionando")
				sys.exit(1)
		else:
			print(utils.FAIL+"La dirección",onionReference,"no es una dirección onion válida")
			sys.exit(1)
		print("-"*30)
			
	# Lectura del fichero con hosts a escanear
	if os.path.exists(inputFileName):
		inputFile = open(inputFileName,'r')
	else:
		print(utils.FAIL+"El fichero de entrada "+inputFileName+" no existe")
		sys.exit(1)

	# Si no existe el path de salida, crearlo
	if not os.path.exists(resultsPath):
		os.mkdir(resultsPath)
	
	# Crear los arrays de sitios vacios
	listaSitesHTTP = []
	listaSitesHTTPs = []
	listaSitesSSH = []
	listaSitesFTP = []

	# En este fichero se añadirán los resultados de la ejecución
	logFile = open('/results/logs', 'a') 
	
	# Leer el fichero de entrada línea a línea
	for hostEscanear in inputFile.read().split('\n'):
		# Si la línea no está vacía, escanear host
		if len(hostEscanear) != 0:
			datosHost = hostEscanear.split(';')
			# Comprobar que el número de parámetros es el esperado
			if (len(datosHost) == 7):
				hostVerificado = True
				# Si se ha escogido verificación, comprobar que los host onion/direcciones IPv4 son válidos
				if onionOnly == 'TRUE':
					# Si el host no cumple los requisitos, no continuar
					if not utils.verify_host(datosHost[0]):
						hostVerificado = False
						print("Host INválido",datosHost[0])
					
				if hostVerificado:
					# Crear si no existe la carpeta destino para este host
					if not os.path.exists("/results/"+datosHost[0]):
						os.mkdir("/results/"+datosHost[0])
					# Crear cadena destino y guardar site
					if(datosHost[1].lower() == 'ssh'):
						results_path = "/results/"+datosHost[0]+"/ssh_"+datosHost[0]+"_"+datosHost[2]
						thisSite = site(datosHost[0],datosHost[1],datosHost[2],datosHost[3],datosHost[4],datosHost[5],datosHost[6],results_path,logFile, userAgent)
						listaSitesSSH.append(thisSite)
					elif(datosHost[1].lower() == 'http'):
						results_path = "/results/"+datosHost[0]+"/http_"+datosHost[0]+"_"+datosHost[2]
						thisSite = site(datosHost[0],datosHost[1],datosHost[2],datosHost[3],datosHost[4],datosHost[5],datosHost[6],results_path,logFile, userAgent)
						listaSitesHTTP.append(thisSite)
					elif(datosHost[1].lower() == 'https'):
						results_path = "/results/"+datosHost[0]+"/https_"+datosHost[0]+"_"+datosHost[2]
						thisSite = site(datosHost[0],datosHost[1],datosHost[2],datosHost[3],datosHost[4],datosHost[5],datosHost[6],results_path,logFile, userAgent)
						listaSitesHTTPs.append(thisSite)
					elif(datosHost[1].lower() == 'ftp'):
						results_path = "/results/"+datosHost[0]+"/ftp_"+datosHost[0]+"_"+datosHost[2]
						thisSite = site(datosHost[0],datosHost[1],datosHost[2],datosHost[3],datosHost[4],datosHost[5],datosHost[6],results_path,logFile, userAgent)
						listaSitesFTP.append(thisSite)
			else:
				print(utils.FAIL+"El servicio NO es válido"+utils.ENDC,hostEscanear)

	# Cerrar fichero de entrada
	inputFile.close()

	# Mostrar resumen de hosts a escanear
	print("Escaneos a realizar con "+str(maxWorkersNum)+" workers")
	print("\t"+str(len(listaSitesSSH))+ " escaneos SSH")
	print("\t"+str(len(listaSitesHTTP))+ " escaneos HTTP")
	print("\t"+str(len(listaSitesHTTPs))+ " escaneos HTTPs")
	print("\t"+str(len(listaSitesFTP))+ " escaneos FTP")
	print("-"*30)

	# sys.exit(1)
	
	# Preparar parámetros para mapearlos con llamadas ThreadPoolExecutor
	elasticHTTP = utils.get_array(es, len(listaSitesHTTP))
	elasticHTTPs = utils.get_array(es, len(listaSitesHTTPs))
	elasticSSH = utils.get_array(es, len(listaSitesSSH))
	elasticFTP = utils.get_array(es, len(listaSitesFTP))
	
	saveFsHttp = utils.get_array(saveFSDataBool, len(listaSitesHTTP))
	saveFsHttps = utils.get_array(saveFSDataBool, len(listaSitesHTTPs))
	saveFsSsh = utils.get_array(saveFSDataBool, len(listaSitesSSH))
	saveFsFtp = utils.get_array(saveFSDataBool, len(listaSitesFTP))
	
	indexEsHttp = utils.get_array(indexDataBool, len(listaSitesHTTP))
	indexEsHttps = utils.get_array(indexDataBool, len(listaSitesHTTPs))
	indexEsSsh = utils.get_array(indexDataBool, len(listaSitesSSH))
	indexEsFtp = utils.get_array(indexDataBool, len(listaSitesFTP))
	
	# Mapear las ejecuciones concurrentes en función del número de workers escogido
	with ThreadPoolExecutor(max_workers=int(maxWorkersNum)) as executor:
		resultsHTTP = executor.map(escaner.escaner_http, listaSitesHTTP, elasticHTTP, saveFsHttp, indexEsHttp)
		resultsHTTPs = executor.map(escaner.escaner_https, listaSitesHTTPs, elasticHTTPs, saveFsHttps, indexEsHttps)
		resultsSSH = executor.map(escaner.escaner_ssh, listaSitesSSH, elasticSSH, saveFsSsh, indexEsSsh)
		resultsFTP = executor.map(escaner.escaner_ftp, listaSitesFTP, elasticFTP, saveFsFtp, indexEsFtp)

	# Cerrar fichero de log
	logFile.close()
	
	# Si no se ha guardado en FileSystem, borrar las carpetas de uso temporal
	if not saveFSDataBool:
		utils.remove_folders(listaSitesHTTP)
		utils.remove_folders(listaSitesHTTPs)
		utils.remove_folders(listaSitesSSH)
		utils.remove_folders(listaSitesFTP)
		
	duration = time.time() - startTime
	print(utils.OKGREEN+"[FINISH] "+str(duration)+" s"+utils.ENDC)
	
#Definición del menu principal
if __name__ == '__main__':
	main()