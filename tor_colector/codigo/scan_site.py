﻿# Métodos en snake case: get_user_name()
# Variables en camel case: userName	 
class site:
	#self.rate 		rate es publica
	#self.__rate 		rate es privada
	def __init__(self, host, service, port, name, description, category, source, resultsPath, logFile, userAgent):
		self.__host = host
		self.__service = service
		self.__port = port
		self.__name = name
		self.__description = description
		self.__category = category
		self.__source = source
		self.__resultsPath = resultsPath
		self.__logFile = logFile
		self.__userAgent = userAgent
		
	def get_host(self):
		return self.__host
		
	def get_service(self):			
		return self.__service
		
	def get_port(self):			
		return self.__port
	
	def get_name(self):			
		return self.__name
	
	def get_description(self):			
		return self.__description
	
	def get_category(self):			
		return self.__category	
			
	def get_source(self):			
		return self.__source	
		
	def get_results_path(self):						
		return self.__resultsPath	
		
	def get_log_file(self):						
		return self.__logFile	
	
	def get_user_agent(self):						
		return self.__userAgent		
	
	def to_string(self):
		return str(('HOST: '+self.__host+' PORT:'+self.__port+' NAME: '+self.__name+' DESCRIPTION: '+self.__description+' SERVICE: '+self.__service+' PATH: '+self.__resultsPath+' LOG: '+self.__logFile.name))